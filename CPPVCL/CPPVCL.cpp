/*
		TASKS

		-	VCLTextGrid
			�	mutex problems (caretRenderMutex), probably rendered 2 times at 1 time, and deadlocks
			�	focus/unfocus problem : VClWindow::WindowProc => VCLRenderqueue::sendMessage.
*/

/*
\***	Made by Cas.
*/

#include "CPPVCL.h"

//***		VCLString

VCLString::VCLString(LPCWSTR text)
	{
		length = wcslen(text);
		string = new wchar_t[length + 1];
		memcpy((void*) string, (void*) text, sizeof(wchar_t) * (length + 1));

		font	= VCLNULL;
	}

VCLString::VCLString(LPCWSTR text, PLOGFONT fntParams)
	{
		length = wcslen(text);
		string = new wchar_t[length + 1];
		memcpy((void*) string, (void*) text, sizeof(wchar_t) * (length + 1));

		font	= CreateFontIndirect(fntParams);
	}

VCLString::~VCLString()
	{
		if(font != VCLNULL)
			DeleteObject(font);

		if(string != VCLNULL)
			delete string;
	}

SIZE VCLString::render(HWND prntWindow, Sprite buffer, int inX, int inY, unsigned int bufWidth, unsigned int bufHeight, VCL_TYPED_HANDLE inCol, unsigned int inClientWidth, unsigned int inClientHeight, int customLen)
	{
		//getting full size of the string in pixels

		SIZE res;
		memset(&res, 0, sizeof(SIZE));

		if(prntWindow != VCLNULL && inX < (int)bufWidth && inY < (int)bufWidth)
			{
				HDC	winDC	= GetDC(prntWindow);
				HDC strDC	= CreateCompatibleDC(winDC);

				int	strX	= 0;
				int strY	= 0;

				SIZE strSize;
				
				if(font != VCLNULL)
					SelectObject(strDC, font);

				if(customLen == 0)
					{
						GetTextExtentPoint32(strDC, string, 1, &res);
						res.cx = 0;
					}
				else if(customLen != -1)
					{
						GetTextExtentPoint32(strDC, string, customLen, &res);
						
						if(res.cx > inClientWidth)
							{
								strX	-= res.cx - inClientWidth;
								res.cx	= inClientWidth;
							}
					}

				GetTextExtentPoint32(strDC, string, length, &strSize);

				if(inX + strSize.cx < 0 || inY + strSize.cy < 0)
					{
						ReleaseDC(prntWindow, winDC);
						DeleteObject(strDC);
						return res;
					}

				BITMAPINFO	bi;

				memset(&bi, 0, sizeof(bi));

				//allocating bitmap by those sizes

				bi.bmiHeader.biSize			= sizeof(bi.bmiHeader);
				bi.bmiHeader.biBitCount		= 32;
				bi.bmiHeader.biWidth		= inClientWidth;
				bi.bmiHeader.biHeight		= -inClientHeight;
				bi.bmiHeader.biCompression	= BI_RGB;
				bi.bmiHeader.biPlanes		= 1;

				Sprite	strSprite;

				//connecting DC to the bitmap and getting its sprite(a pixel array)

				HBITMAP strBmp				= CreateDIBSection(strDC, &bi, DIB_RGB_COLORS, (void**)&(strSprite), NULL, 0);
        
				SelectObject(strDC, strBmp);

				//getting length of the visual part string array 

				unsigned int clientLen		= inClientWidth * inClientHeight;

				//initializing it with all alphas - 255

				for(unsigned int index = 0; index < clientLen; index++)
					strSprite[index].a = 255;			

				//drawing text shape on that bitmap (all text pixels will have 0 alpha)

				SetBkMode(strDC, TRANSPARENT);
				SetTextColor(strDC, GET_PIX_TRANSP_COLOR(0, 0, 0, 255));
				TextOut(strDC, strX, strY, string, length);

				//getting start positions: 
				//clientPos - invisual part string array; 
				//endPos	- by the caption coordinates on the button

				unsigned int clientPos			= 0;
				unsigned int spPos				= inY * bufWidth + inX;

				//going while we dont reach the end of visual string buffer

				if(inCol.type == VCL_HT_RGBA_COLOR)
					{
						while(clientPos < clientLen)
							{
								//going throught visual string buffer line
								for(unsigned int xindex = 0; xindex < inClientWidth; xindex++)
									{
										//if its text pixel - draw it
										if(!strSprite[clientPos].a)
											{
												OPT_SET_TRANSP_PIX	(
																		spPos,
																		((RGBAPix*)(inCol.handle))->b,
																		((RGBAPix*)(inCol.handle))->g,
																		((RGBAPix*)(inCol.handle))->r,
																		((RGBAPix*)(inCol.handle))->a,
																		buffer
																	);
											}

										spPos++;
										clientPos++;
									}

								//going on next line to the same X position

								spPos			+= bufWidth		- inClientWidth;
							}
					}
				else if(inCol.type == VCL_HT_GENERIC_COLOR)
					{
						RGBAPix tmpCol;

						while(clientPos < clientLen)
							{
								//going throught visual string buffer line
								for(unsigned int xindex = 0; xindex < inClientWidth; xindex++)
									{
										//if its text pixel - draw it
										if(!strSprite[clientPos].a)
											{
												tmpCol = ((VCLGenericColor*)(inCol.handle))->getColor(spPos % bufWidth, spPos / bufWidth, bufWidth, bufHeight);

												OPT_SET_TRANSP_PIX	(
																		spPos,
																		tmpCol.b,
																		tmpCol.g,
																		tmpCol.r,
																		tmpCol.a,
																		buffer
																	);
											}

										spPos++;
										clientPos++;
									}

								//going on next line to the same X position

								spPos			+= bufWidth		- inClientWidth;
							}
					}

				//releasing help stuff

				ReleaseDC(prntWindow, winDC);
				DeleteDC(strDC);
				DeleteObject(strBmp);
			}

		return res;
	}

unsigned int VCLString::getLength()
	{
		return length;
	}

LPCWSTR	VCLString::getText()
	{
		return string;
	}

void VCLString::setText(LPCWSTR newText)
	{
		length = wcslen(newText);
		string = new wchar_t[length];
		memcpy((void*) string, (void*) newText, sizeof(wchar_t) * length);
	}

void VCLString::addChar(wchar_t inChar)
	{
		LPWSTR newStr = new wchar_t[++length + 1];

		memcpy(newStr, string, sizeof(wchar_t) * (length - 1));
		newStr[length - 1]		= inChar;
		newStr[length]		= 0;

		delete string;

		string = newStr;
	}

void VCLString::delChar(unsigned int inIndex)
	{
		if(length > 0 && inIndex < length)
			{
				LPWSTR newStr = new wchar_t[--length + 1];

				memcpy(newStr, string, sizeof(wchar_t) * inIndex);
				memcpy(&newStr[inIndex], &string[inIndex + 1], sizeof(wchar_t) * (length - inIndex));
				
				newStr[length]		= 0;

				delete string;

				string = newStr;
			}
	}

void VCLString::insChar(unsigned int inIndex, wchar_t inChar)
	{
		LPWSTR newStr = new wchar_t[++length + 1];

		memcpy(newStr, string, sizeof(wchar_t) * inIndex);
		newStr[inIndex]		= inChar;
		memcpy(&newStr[inIndex + 1], &string[inIndex], sizeof(wchar_t) * (length - inIndex));
		newStr[length]		= 0;

		delete string;

		string = newStr;
	}

void VCLString::concat(LPCWSTR inStr)
	{
		unsigned int inLen = wcslen(inStr);

		LPWSTR newStr = new wchar_t[inLen + length + 1];

		memcpy(newStr, string, sizeof(wchar_t) * length);
		memcpy(&newStr[length], inStr, sizeof(wchar_t) * inLen);

		length += inLen - 1;

		newStr[length] = 0;

		delete string;

		string = newStr;
	}

void VCLString::setFont(PLOGFONT	fntParams)
	{
		if(font != VCLNULL)
			DeleteObject(font);

		font	= CreateFontIndirect(fntParams);
	}

//***		VCLGenericColor

VCLGenericColor::VCLGenericColor()
	{
		b = VCLNULL;
		g = VCLNULL;
		r = VCLNULL;
		a = VCLNULL;
	}

VCLGenericColor::~VCLGenericColor()
	{
		releaseExpression(b);
		releaseExpression(g);
		releaseExpression(r);
		releaseExpression(a);
	}

void VCLGenericColor::initGenericColor(const char* bExpr, const char* gExpr, const char* rExpr, const char* aExpr)
	{
		releaseExpression(b);
		releaseExpression(g);
		releaseExpression(r);
		releaseExpression(a);

		unsigned int reserved;

		reserved = 0;

		b = parseExpression(bExpr, strlen(bExpr), &reserved);
	
		reserved = 0;

		g = parseExpression(gExpr, strlen(gExpr), &reserved);

		reserved = 0;

		r = parseExpression(rExpr, strlen(rExpr), &reserved);

		reserved = 0;

		a = parseExpression(aExpr, strlen(aExpr), &reserved);
	}

void VCLGenericColor::releaseExpression(VCL_EXPRESSION_ELEM* head)
	{
		if(head != VCLNULL)
			{
				VCL_BRANCH* tmpBranch = head->branches;

				while(tmpBranch != VCLNULL)
					{
						releaseExpression(tmpBranch->elem);

						tmpBranch = tmpBranch->next;
					}

				delete head;
			}
	}

VCL_BYTE_PAIR VCLGenericColor::isOperator(const char* obj)
	{
		VCL_BYTE_PAIR res;

		res._1 = VCL_NO_OP;

		if(		!strcmp(obj, "+")	)
			{
				res._1 = VCL_OP_PLUS;
				res._2 = 2;
			}
		else if(!strcmp(obj, "-")	)
			{
				res._1 = VCL_OP_MINUS;
				res._2 = 2;
			}
		else if(!strcmp(obj, "/")	)
			{
				res._1 = VCL_OP_DIV;
				res._2 = 2;
			}
		else if(!strcmp(obj, "*")	)
			{
				res._1 = VCL_OP_MUL;
				res._2 = 2;
			}
		else if(!strcmp(obj, "%")	)
			{
				res._1 = VCL_OP_MOD;
				res._2 = 2;
			}
		else if(!strcmp(obj, "sin")	)
			{
				res._1 = VCL_OP_SIN;
				res._2 = 1;
			}
		else if(!strcmp(obj, "tg")	)
			{
				res._1 = VCL_OP_TG;
				res._2 = 1;
			}
		else if(!strcmp(obj, "sqrt"))
			{
				res._1 = VCL_OP_SQRT;
				res._2 = 1;
			}
		else if(!strcmp(obj, "pow")	)
			{
				res._1 = VCL_OP_POW;
				res._2 = 2;
			}
		else if(!strcmp(obj, "int")	)
			{
				res._1 = VCL_OP_INT;
				res._2 = 1;
			}
		else if(!strcmp(obj, "~")	)
			{
				res._1 = VCL_OP_BIT_NO;
				res._2 = 1;
			}
		else if(!strcmp(obj, ">>")	)
			{
				res._1 = VCL_OP_BIT_R_OFF;
				res._2 = 2;
			}
		else if(!strcmp(obj, "<<")	)
			{
				res._1 = VCL_OP_BIT_L_OFF;
				res._2 = 2;
			}
		else if(!strcmp(obj, "&")	)
			{
				res._1 = VCL_OP_BIT_AND;
				res._2 = 2;
			}
		else if(!strcmp(obj, "|")	)
			{
				res._1 = VCL_OP_BIT_OR;
				res._2 = 2;
			}
		else if(!strcmp(obj, "^")	)
			{
				res._1 = VCL_OP_BIT_XOR;
				res._2 = 2;
			}
		else if(!strcmp(obj, "x")	)
			{
				res._1 = VCL_VAR_X;
				res._2 = 0;
			}
		else if(!strcmp(obj, "y")	)
			{
				res._1 = VCL_VAR_Y;
				res._2 = 0;
			}
		else if(!strcmp(obj, "width")	)
			{
				res._1 = VCL_VAR_WIDTH;
				res._2 = 0;
			}
		else if(!strcmp(obj, "height")	)
			{
				res._1 = VCL_VAR_HEIGHT;
				res._2 = 0;
			}

		return res;
	}

bool VCLGenericColor::isNumeric(const char* obj)
	{
		bool			res = true;
		unsigned int	len = strlen(obj);

		for(unsigned int index = 0; index < len; index++)
			{
				if((obj[index] < 48 || obj[index] > 58) && obj[index] != 46)
					{
						res = false;
						break;
					}
			}

		return res;
	}

VCL_EXPRESSION_ELEM* VCLGenericColor::parseExpression(const char* expr, unsigned int exprLen, unsigned int* reserved)
	{
		VCL_EXPRESSION_ELEM*	newElem = VCLNULL;

		char					expObj[30];
		unsigned int			objIndex = 0;

		while(expr[*reserved] != 32 && *reserved < exprLen)
			expObj[objIndex++] = expr[(*reserved)++];

		expObj[objIndex] = 0;

		VCL_BYTE_PAIR opType = isOperator(expObj);

		if(opType._1 != VCL_NO_OP)
			{
				newElem					= new VCL_EXPRESSION_ELEM;
				newElem->type			= opType._1;
				newElem->info			= VCLNULL;

				newElem->branches		= VCLNULL;

				VCL_BRANCH* tmpBranch	= VCLNULL;

				unsigned int reservedBeforeLastObj;

				for(unsigned int index = 0; index < opType._2; index++)
					{
						if(newElem->branches == VCLNULL)
							{
								newElem->branches = new VCL_BRANCH;
								newElem->branches->next = VCLNULL;

								tmpBranch = newElem->branches;
							}
						else
							{
								tmpBranch->next = new VCL_BRANCH;

								tmpBranch = tmpBranch->next;
								//tmpBranch->next = VCLNULL;
							}

						//***

						objIndex = 0;

						reservedBeforeLastObj = ++(*reserved);

						while(expr[*reserved] != 32 && *reserved < exprLen)
							expObj[objIndex++] = expr[(*reserved)++];

						expObj[objIndex] = 0;

						if(isNumeric(expObj))
							{
								tmpBranch->elem						= new VCL_EXPRESSION_ELEM;
								tmpBranch->elem->type				= VCL_FNUM;
								tmpBranch->elem->info				= new float;
								*((float*)(tmpBranch->elem->info))	= atof(expObj);
								tmpBranch->elem->branches			= VCLNULL;
							}
						else
							{
								*reserved = reservedBeforeLastObj;
								tmpBranch->elem							= parseExpression(expr, exprLen, reserved);
							}
					}

				if(tmpBranch != VCLNULL)
					tmpBranch->next = VCLNULL;
			}
		
		return newElem;
	}

float VCLGenericColor::calculateExpression(VCL_EXPRESSION_ELEM* head, unsigned int x, unsigned int y, unsigned int width, unsigned int height)
	{
		float res = 0;

		if(head != VCLNULL)
			{
				if(head->type > VCL_LAST_NUM)
					{
						switch(head->type)
							{
								//**		operations check

								case VCL_OP_NOT:
									{
										res = -(calculateExpression(head->branches->elem, x, y, width, height));

										break;
									}
								case VCL_OP_PLUS:
									{
										VCL_BRANCH* curr = head->branches;

										while(curr != VCLNULL)
											{
												res += calculateExpression(curr->elem, x, y, width, height);

												curr = curr->next;
											}
										
										break;
									}
								case VCL_OP_MINUS:
									{
										VCL_BRANCH* curr = head->branches;

										res = calculateExpression(curr->elem, x, y, width, height);
												
										curr = curr->next;

										while(curr != VCLNULL)
											{
												res -= calculateExpression(curr->elem, x, y, width, height);

												curr = curr->next;
											}
										
										break;
									}
								case VCL_OP_DIV:
									{
										VCL_BRANCH* curr = head->branches;

										res = calculateExpression(curr->elem, x, y, width, height);
												
										curr = curr->next;

										while(curr != VCLNULL)
											{
												res /= calculateExpression(curr->elem, x, y, width, height);

												curr = curr->next;
											}
										
										break;
									}
								case VCL_OP_MUL:
									{
										VCL_BRANCH* curr = head->branches;

										res = calculateExpression(curr->elem, x, y, width, height);
												
										curr = curr->next;

										while(curr != VCLNULL)
											{
												res *= calculateExpression(curr->elem, x, y, width, height);

												curr = curr->next;
											}											
										
										break;
									}
								case VCL_OP_MOD:
									{
										VCL_BRANCH* curr = head->branches;

										res = calculateExpression(curr->elem, x, y, width, height);
												
										curr = curr->next;

										while(curr != VCLNULL)
											{
												res = (int)(res) % (int)(calculateExpression(curr->elem, x, y, width, height));

												curr = curr->next;
											}
										
										break;
									}
								case VCL_OP_SIN:
									{
										res = sin(calculateExpression(head->branches->elem, x, y, width, height));
										
										break;
									}
								case VCL_OP_TG:
									{
										res = tan(calculateExpression(head->branches->elem, x, y, width, height));
										
										break;
									}
								case VCL_OP_SQRT:
									{
										res = sqrt(calculateExpression(head->branches->elem, x, y, width, height));
										
										break;
									}
								case VCL_OP_POW:
									{
										res = pow	(
														calculateExpression(head->branches->elem,		x, y, width, height),
														calculateExpression(head->branches->next->elem, x, y, width, height)
													);
										
										break;
									}
								case VCL_OP_INT:
									{
										res = (int)(calculateExpression(head->branches->elem, x, y, width, height));
										
										break;
									}
								case VCL_OP_BIT_NO:
									{
										res = ~(int)(calculateExpression(head->branches->elem, x, y, width, height));
										
										break;
									}
								case VCL_OP_BIT_R_OFF:
									{
										res = (int)(calculateExpression(head->branches->elem, x, y, width, height)) >> (int)(calculateExpression(head->branches->next->elem, x, y, width, height));
										
										break;
									}
								case VCL_OP_BIT_L_OFF:
									{
										res = (int)(calculateExpression(head->branches->elem, x, y, width, height)) << (int)(calculateExpression(head->branches->next->elem, x, y, width, height));
										
										break;
									}
								case VCL_OP_BIT_AND:
									{
										res = (int)(calculateExpression(head->branches->elem, x, y, width, height)) & (int)(calculateExpression(head->branches->next->elem, x, y, width, height));
										
										break;
									}
								case VCL_OP_BIT_OR:
									{
										res = (int)(calculateExpression(head->branches->elem, x, y, width, height)) | (int)(calculateExpression(head->branches->next->elem, x, y, width, height));
										
										break;
									}
								case VCL_OP_BIT_XOR:
									{
										res = (int)(calculateExpression(head->branches->elem, x, y, width, height)) ^ (int)(calculateExpression(head->branches->next->elem, x, y, width, height));
										
										break;
									}

								//**		vars check

								case VCL_VAR_X:
									{
										res = x;
										
										break;
									}
								case VCL_VAR_Y:
									{
										res = y;
										
										break;
									}
								case VCL_VAR_WIDTH:
									{
										res = width;
										
										break;
									}
								case VCL_VAR_HEIGHT:
									{
										res = height;
										
										//break;
									}
							}
					}
				else
					res = *((float*)(head->info));
			}

		return res;
	}

RGBAPix	VCLGenericColor::getColor(unsigned int x, unsigned int y, unsigned int width, unsigned int height)
	{
		RGBAPix res;

		res.b = calculateExpression(b, x, y, width, height);
		res.g = calculateExpression(g, x, y, width, height);
		res.r = calculateExpression(r, x, y, width, height);
		res.a = calculateExpression(a, x, y, width, height);

		return res;
	}

//***		VCLComponent

VCLComponent::VCLComponent	(
								HWND			inParentWindow,
								VCLComponent*	inParentComponent,
								VCLRenderQueue*	inRenderList,
								unsigned int	inX,
								unsigned int	inY,
								unsigned int	inWidth,
								unsigned int	inHeight,
								char			inVisible
							)
	{
		parentComponent			= inParentComponent;

		if(inParentWindow == NULL)
			{
				VCLComponent*	tmpComp = inParentComponent;

				while(tmpComp->parentComponent != VCLNULL)
					tmpComp = tmpComp->parentComponent;

				parentWindow = tmpComp->parentComponent->parentWindow;
			}
		else
			parentWindow		= inParentWindow;

		renderList				= inRenderList;
		x						= inX;
		y						= inY;
		width					= inWidth;
		height					= inHeight;

		sprite					= VCLNULL;

		visible					= inVisible;

		MouseLDownHandle		= VCLNULL;
		MouseRDownHandle		= VCLNULL;
		MouseMDownHandle		= VCLNULL;
		MouseLUpHandle			= VCLNULL;
		MouseRUpHandle			= VCLNULL;
		MouseMUpHandle			= VCLNULL;
		MouseMoveHandle			= VCLNULL;
		MouseScrollDownHandle	= VCLNULL;
		MouseScrollUpHandle		= VCLNULL;
		KeyDownHandle			= VCLNULL;
		KeyUpHandle				= VCLNULL;
		SelectHandle			= VCLNULL;
		UnselectHandle			= VCLNULL;

		next					= VCLNULL;
		prev					= VCLNULL;

		id						= renderList->getNewId();

		rebuildSprite(VCLALLOC);
	}

VCLComponent::~VCLComponent()
	{
		hide();

		if(parentWindow != VCLNULL)
			{
				DeleteDC(localDC);
				DeleteObject(localBmp);
			}
	}

//*** memory allocation without any initialization (just for base class)

void VCLComponent::rebuildSprite(char realloc)
	{	
		if(parentWindow != VCLNULL)
			{
				if(sprite != VCLNULL && realloc == VCLREALLOC)
					{
						DeleteDC(localDC);
						DeleteObject(localBmp);
					}

				if(realloc != VCLNOALLOC)
					{
						HDC	winDC = GetDC(parentWindow);

						localDC = CreateCompatibleDC(winDC);

						BITMAPINFO	bi;

						memset(&bi, 0, sizeof(bi));

						bi.bmiHeader.biSize			= sizeof(bi.bmiHeader);
						bi.bmiHeader.biBitCount		= 32;
						bi.bmiHeader.biWidth		= width;
						bi.bmiHeader.biHeight		= -height;
						bi.bmiHeader.biCompression	= BI_RGB;
						bi.bmiHeader.biPlanes		= 1;

						localBmp = CreateDIBSection(localDC, &bi, DIB_RGB_COLORS, (void**)&(sprite), NULL, 0);
        
						SelectObject(localDC, localBmp);

						SetBkMode(localDC, TRANSPARENT);

						ReleaseDC(parentWindow, winDC);
					}
			}
	}

void VCLComponent::refresh()
	{
		rebuildSprite(VCLNOALLOC);
	}

//***		just copying the visual part of the sprite into the renderList buffer

void VCLComponent::render()
	{
		//**		We could use this if we dont need transparency stuff, but we need it :)
		//BitBlt(renderList->memdc, x, y, width, height, localDC, 0, 0, SRCCOPY);

		if	(
				x >= renderList->clientRect.right	||
				y >= renderList->clientRect.bottom	||
				x + (int)width	<= 0				||
				y + (int)height	<= 0
			)
			return;

		int posX;
		int posY;

		unsigned int localX;
		unsigned int localY;

		unsigned int localWidth		= width;
		unsigned int localHeight	= height;

		if(x < 0)
			{
				posX		= 0;
				localWidth	+= x;
				localX		= -x;
			}
		else
			{
				posX		= x;
				localX		= 0;
			}

		if(y < 0)
			{
				posY		= 0;
				localHeight += y;
				localY		= -y;
			}
		else
			{
				posY		= y;
				localY		= 0;
			}

		if(posX + localWidth > renderList->clientRect.right)
			localWidth				= renderList->clientRect.right - posX;

		if(posY + localHeight > renderList->clientRect.bottom)
			localHeight				= renderList->clientRect.bottom - posY;

		unsigned int pos			= posY * renderList->clientRect.right + posX;
		unsigned int localPos		= localY * width + localX;

		unsigned int offset			= renderList->clientRect.right - localWidth;
		unsigned int localOffset	= width - localWidth;

		unsigned int lastLine		= (localY+localHeight) * width;

		while(localPos < lastLine)
			{
				for(unsigned int xStep = localX; xStep < localX+localWidth; xStep++)
					{
						OPT_SET_TRANSP_PIX	(
												pos,
												sprite[localPos].b,
												sprite[localPos].g,
												sprite[localPos].r,
												sprite[localPos].a,
												renderList->hiddenBuffer
											);

						localPos++;
						pos++;
					}
				
				pos			+= offset;
				localPos	+= localOffset;
			}
		
	}

void VCLComponent::focus()
	{
		renderList->remove(this);
		renderList->addToEnd(this);
		renderList->selectComp(this);

		if(SelectHandle != VCLNULL)
			SelectHandle(this);
	}

void VCLComponent::unfocus()
	{
		renderList->selectComp(VCLNULL);

		if(UnselectHandle != VCLNULL)
			UnselectHandle(this);
	}

void VCLComponent::update()
	{
		if(parentWindow != NULL)
			SendMessage(parentWindow, WM_PAINT, NULL, NULL);
	}

void VCLComponent::show()
	{
		if(visible != VCLACCEPT)
			{
				visible = VCLACCEPT;

				renderList->addToEnd(this);
			}
	}

void VCLComponent::hide()
	{
		if(visible != VCLNULL)
			{
				visible = VCLNULL;			

				renderList->remove(this);
			}
	}

void VCLComponent::setPos(int newX, int newY)
	{
		x		= newX;
		y		= newY;
	}

void VCLComponent::setSize(unsigned int newWdh, unsigned int newHgh)
	{
		width	= newWdh;
		height	= newHgh;

		rebuildSprite(VCLREALLOC);
	}

int VCLComponent::getX()
	{
		return x;
	}

int VCLComponent::getY()
	{
		return y;
	}

unsigned int VCLComponent::getWidth()
	{
		return width;
	}

unsigned int VCLComponent::getHeight()
	{
		return height;
	}

unsigned int VCLComponent::getId()
	{
		return id;
	}

char VCLComponent::isVisible()
	{
		return visible;
	}

char VCLComponent::isSelected()
	{
		if(renderList != VCLNULL)
			return renderList->getSelectedComp() == this;
		else
			return VCLNULL;
	}

//***		VCLRenderQueue

VCLRenderQueue::VCLRenderQueue()
	{
		start			= VCLNULL;
		end				= VCLNULL;

		selectedComp	= VCLNULL;

		generator		= 0;
	}

VCLRenderQueue::~VCLRenderQueue()
	{
		freeList();
	}

void VCLRenderQueue::addToStart(VCLComponent* newComponent)
	{
		newComponent->prev = VCLNULL;

		if(start != VCLNULL)
			{
				newComponent->next	= start;
				start->prev			= newComponent;

				start				= newComponent;
			}
		else
			{
				newComponent->next	= VCLNULL;
				start				= newComponent;
				end					= newComponent;
			}
	}

void VCLRenderQueue::addToEnd(VCLComponent* newComponent)
	{
		newComponent->next = VCLNULL;

		if(end != VCLNULL)
			{
				newComponent->prev	= end;
				end->next			= newComponent;

				end					= newComponent;
			}
		else
			{
				newComponent->prev	= VCLNULL;
				end					= newComponent;
				start				= newComponent;
			}
	}

//***			Why do we need id for deleting?!? We are
//***			already have an instance of this object,
//***			so no need to search! :)

void VCLRenderQueue::remove(VCLComponent* oldComponent)
	{
		if(oldComponent == start)
			start = start->next;

		if(oldComponent == end)
			end = end->prev;

		if(oldComponent->prev != VCLNULL)
			oldComponent->prev->next = oldComponent->next;

		if(oldComponent->next != VCLNULL)
			oldComponent->next->prev = oldComponent->prev;
	}

void VCLRenderQueue::freeList()
	{
		while(start != VCLNULL)
			{
				end = start;

				start = start->next;

				delete end;
			}

		end = VCLNULL;
	}

void VCLRenderQueue::renderList()
	{
		VCLComponent* current = start;

		while(current != VCLNULL)
			{
				current->render();

				current = current->next;
			}
	}

//***		basicly parse WinAPI message code, and send translated message to the component
//***		?Do we need to send message throught the queue until some component will stop it?

char VCLRenderQueue::sendMessage(unsigned int msg, unsigned int wParam, unsigned int lParam)
	{
		VCLComponent* curr = end;

		if(msg == VCL_LBTN_UP)
			msg = VCL_LBTN_UP;

		while(curr != VCLNULL && curr->react(msg, wParam, lParam) != VCLACCEPT)
			curr = curr->prev;

		return (char)curr;
	}

VCLComponent* VCLRenderQueue::getSelectedComp()
	{
		return selectedComp;
	}

void VCLRenderQueue::selectComp(VCLComponent* inComponent)
	{
		selectedComp = inComponent;
	}

VCLComponent* VCLRenderQueue::getFirstInList()
	{
		return start;
	}

VCLComponent* VCLRenderQueue::getLastInList()
	{
		return end;
	}

unsigned int VCLRenderQueue::getNewId()
	{
		return generator++;
	}

//***			VCLWindow

VCLWindow::VCLWindow(VCL_WINDOW_CREATE_PARAMS* wndParams)
	{
		VCL_WINDOW_THREAD_METHOD_PARAM*	mParam = new VCL_WINDOW_THREAD_METHOD_PARAM;

		mParam->method	= &VCLWindow::processMessages;
		mParam->object	= this;
		mParam->param	= wndParams;

		width			= wndParams->wndWidth;
		height			= wndParams->wndHeight;
		x				= wndParams->wndX;
		y				= wndParams->wndY;
		bkColor			= wndParams->wndBkColor;

		WindowRenderHandle = VCLNULL;

		windowMutex		= CreateMutex(NULL, false, NULL);
		renderMutex		= CreateMutex(NULL, false, NULL);

		closed			= VCLOPENING;

		compList		= new VCLRenderQueue();

		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)newThreadMethod, mParam, 0, NULL);

		while(closed != VCLOPENED && closed != VCLCLOSED);
	}

//***		used for run some window methods in different threads

DWORD VCLWindow::newThreadMethod(VCL_WINDOW_THREAD_METHOD_PARAM* mParam)
	{
		VCLWindow*					pObject = mParam->object;
		VCL_WINDOW_THREAD_METHOD	pMethod = mParam->method;
		LPVOID						pParam	= mParam->param;

		DWORD						res		= (pObject->*pMethod)(pParam);

		delete mParam;
		return res;
	}

//***		This method keep main window thread alive, till it will get close message etc.

void VCLWindow::windowLiveCycle()
	{
		while(true)
			{
				WaitForSingleObject(windowMutex, INFINITE);

				if(closed == VCLCLOSED)
					{
						ReleaseMutex(windowMutex);
						break;
					}

				ReleaseMutex(windowMutex);
			}
	}

//***		Basicly window creation thread, and messages process thread.
//***		Each window have its own thread.

unsigned int VCLWindow::processMessages(LPVOID lParam)
	{
		VCL_WINDOW_CREATE_PARAMS*	cParam = (VCL_WINDOW_CREATE_PARAMS*)(lParam);

		WNDCLASSEX					windowClassStruct;
		
		windowClassStruct.cbSize		= sizeof(windowClassStruct);
		windowClassStruct.style			= CS_BYTEALIGNCLIENT;
		windowClassStruct.lpfnWndProc	= WindowProc;
		windowClassStruct.cbClsExtra	= 0;
		windowClassStruct.cbWndExtra	= sizeof(LONG_PTR);
		windowClassStruct.hInstance		= cParam->hInst;
		windowClassStruct.hIcon			= NULL;
		windowClassStruct.hCursor		= LoadCursor(NULL, IDC_ARROW);
		windowClassStruct.hbrBackground = (HBRUSH)COLOR_APPWORKSPACE;
		windowClassStruct.lpszMenuName	= NULL;
		windowClassStruct.hIconSm		= NULL;
		windowClassStruct.lpszClassName = cParam->wndClassName;

		/*if(!RegisterClassEx(&windowClassStruct))
			return NULL;
		
		But what if error in first creation of class?!?

		*/

		RegisterClassEx(&windowClassStruct);

		hWindow = CreateWindow	(
										cParam->wndClassName,
										cParam->wndCaption,
										cParam->wndStyle,
										x,
										y,
										width,
										height,
										NULL,
										NULL,
										cParam->hInst,
										this
									);

		closed = VCLOPENED;

		SetWindowLongPtr(hWindow, 0, (LONG)this);
	 
		if(!hWindow)
			return NULL;

		ShowWindow	(hWindow, cParam->nCommandShow);
		UpdateWindow(hWindow);

		MSG		message;

		while(GetMessage(&message, NULL, NULL, NULL))
			{
				TranslateMessage(&message);
				DispatchMessage(&message);
			}

		WaitForSingleObject(windowMutex, INFINITE);

		closed = VCLCLOSED;

		ReleaseMutex(windowMutex);

		return message.wParam;
	}

VCLWindow::~VCLWindow()
	{
		delete compList;

		DeleteDC(compList->memdc);
		DeleteObject(hMemBmp);
		ReleaseDC(hWindow, hdc);
	}

//***		static function that handle all messages incomping to each window created here
//***		for get non-static fields used extra window memory to write in a VCLWindow pointer
//***		so it can get any public field from the window

LRESULT __stdcall VCLWindow::WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		VCLWindow* vclWnd = (VCLWindow*) GetWindowLongPtr(hWnd, 0);

		switch(uMsg)
			{
				case WM_PAINT:
					{
						if(!(vclWnd->compList->clientRect.right && vclWnd->compList->clientRect.bottom))
							break;

						WaitForSingleObject(vclWnd->windowMutex, INFINITE);

						if(vclWnd->closed == VCLOPENED)
							{
								WaitForSingleObject(vclWnd->renderMutex, INFINITE);
		
								if(vclWnd->closed != VCLOPENED)
									{
										ReleaseMutex(vclWnd->renderMutex);
										ReleaseMutex(vclWnd->windowMutex);
										return NULL;
									}

								//***	render here
								
								//**	background filling
								
								if(vclWnd->bkColor.type == VCL_HT_RGBA_COLOR)
									{
										for(int index = 0; index < vclWnd->compList->clientRect.right * vclWnd->compList->clientRect.bottom; index++)
											vclWnd->compList->hiddenBuffer[index] = *((RGBAPix*)(vclWnd->bkColor.handle));
									}
								else if(vclWnd->bkColor.type == VCL_HT_GENERIC_COLOR)
									{
										for(int index = 0; index < vclWnd->compList->clientRect.right * vclWnd->compList->clientRect.bottom; index++)
											vclWnd->compList->hiddenBuffer[index] = ((VCLGenericColor*)(vclWnd->bkColor.handle))->getColor	(
																																				index % vclWnd->compList->clientRect.right,
																																				index / vclWnd->compList->clientRect.right,
																																				vclWnd->compList->clientRect.right,
																																				vclWnd->compList->clientRect.bottom
																																			);
									}
								//**	render components

								vclWnd->compList->renderList();

								//***

								if(vclWnd->WindowRenderHandle != VCLNULL)
									vclWnd->WindowRenderHandle(vclWnd);

								BitBlt(vclWnd->hdc, 0, 0, vclWnd->compList->clientRect.right, vclWnd->compList->clientRect.bottom, vclWnd->compList->memdc, 0, 0, SRCCOPY);

								ReleaseMutex(vclWnd->renderMutex);
								ReleaseMutex(vclWnd->windowMutex);
							}
						else
							{
								vclWnd->closed = VCLCLOSING;
								ReleaseMutex(vclWnd->windowMutex);

								CloseHandle(vclWnd->windowMutex);
							}

						break;
					}
				case WM_MOUSEMOVE:
					{
						if(vclWnd != NULL && vclWnd->compList != VCLNULL)
							{
								vclWnd->compList->sendMessage(VCL_MMOVE, wParam, lParam);
									/*{
										if(vclWnd->compList->getSelectedComp() != VCLNULL)
											{
												vclWnd->compList->getSelectedComp()->unfocus();
												//SendMessage(hWnd, WM_PAINT, NULL, NULL);
											}
									}*/
							}

						break;
					}
				case WM_LBUTTONDOWN:
					{
						if(vclWnd != NULL && vclWnd->compList != VCLNULL)
							{
								if(vclWnd->compList->sendMessage(VCL_LBTN_DOWN, wParam, lParam) == VCLNULL)
									{
										if(vclWnd->compList->getSelectedComp() != VCLNULL)
											{
												vclWnd->compList->getSelectedComp()->unfocus();
												SendMessage(hWnd, WM_PAINT, NULL, NULL);
											}
									}
							}

						break;
					}
				case WM_LBUTTONUP:
					{
						if(vclWnd != NULL && vclWnd->compList != VCLNULL)
							{
								if(vclWnd->compList->sendMessage(VCL_LBTN_UP, wParam, lParam) == VCLNULL)
									{
										/*if(vclWnd->compList->getSelectedComp() != VCLNULL)
											{
												vclWnd->compList->getSelectedComp()->unfocus();
												SendMessage(hWnd, WM_PAINT, NULL, NULL);
											}*/
									}
							}

						break;
					}
				case WM_RBUTTONDOWN:
					{
						if(vclWnd != NULL && vclWnd->compList != VCLNULL)
							{
								if(vclWnd->compList->sendMessage(VCL_RBTN_DOWN, wParam, lParam) == VCLNULL)
									{
										/*if(vclWnd->compList->getSelectedComp() != VCLNULL)
											{
												vclWnd->compList->getSelectedComp()->unfocus();
												SendMessage(hWnd, WM_PAINT, NULL, NULL);
											}*/
									}
							}

						break;
					}
				case WM_RBUTTONUP:
					{
						if(vclWnd != NULL && vclWnd->compList != VCLNULL)
							{
								if(vclWnd->compList->sendMessage(VCL_RBTN_UP, wParam, lParam) == VCLNULL)
									{
										/*if(vclWnd->compList->getSelectedComp() != VCLNULL)
											{
												vclWnd->compList->getSelectedComp()->unfocus();
												SendMessage(hWnd, WM_PAINT, NULL, NULL);
											}*/
									}
							}

						break;
					}
				case WM_MBUTTONDOWN:
					{
						if(vclWnd != NULL && vclWnd->compList != VCLNULL)
							{
								if(vclWnd->compList->sendMessage(VCL_MBTN_DOWN, wParam, lParam) == VCLNULL)
									{
										/*if(vclWnd->compList->getSelectedComp() != VCLNULL)
											{
												vclWnd->compList->getSelectedComp()->unfocus();
												SendMessage(hWnd, WM_PAINT, NULL, NULL);
											}*/
									}
							}

						break;
					}
				case WM_MBUTTONUP:
					{
						if(vclWnd != NULL && vclWnd->compList != VCLNULL)
							{
								if(vclWnd->compList->sendMessage(VCL_MBTN_UP, wParam, lParam) == VCLNULL)
									{
										/*if(vclWnd->compList->getSelectedComp() != VCLNULL)
											{
												vclWnd->compList->getSelectedComp()->unfocus();
												SendMessage(hWnd, WM_PAINT, NULL, NULL);
											}*/
									}
							}

						break;
					}
				case WM_KEYDOWN:
					{
						if(vclWnd != NULL && vclWnd->compList != VCLNULL)
							{
								vclWnd->compList->sendMessage(VCL_KEY_DOWN, wParam, lParam);
								/*if(vclWnd->compList->getSelectedComp() != VCLNULL)
									{
										vclWnd->compList->getSelectedComp()->unfocus();
										//SendMessage(hWnd, WM_PAINT, NULL, NULL);
									}*/
							}

						break;
					}
				case WM_CHAR:
					{
						if(vclWnd != NULL && vclWnd->compList != VCLNULL)
							vclWnd->compList->sendMessage(VCL_CHAR, wParam, lParam);

						break;
					}
				case WM_KEYUP:
					{
						if(vclWnd != NULL && vclWnd->compList != VCLNULL)
							vclWnd->compList->sendMessage(VCL_KEY_UP, wParam, lParam);

						break;
					}
				case WM_MOUSEWHEEL:
					{
						POINT mPos;

						mPos.x = (short)(lParam);
						mPos.y = (short)(lParam >> 16);

						ScreenToClient(hWnd, &mPos);

						if(vclWnd != NULL && vclWnd->compList != VCLNULL)
							vclWnd->compList->sendMessage(VCL_SCROLL, wParam, (unsigned int)&mPos);

						break;
					}
				case WM_SIZE:
					{
						DeleteDC(vclWnd->compList->memdc);
						DeleteObject(vclWnd->hMemBmp);
						ReleaseDC(hWnd, vclWnd->hdc);

						GetClientRect(hWnd, &vclWnd->compList->clientRect);

						vclWnd->hdc		= GetDC(hWnd);
						vclWnd->compList->memdc	= CreateCompatibleDC(vclWnd->hdc);
						
						BITMAPINFO	bi;

						memset(&bi, 0, sizeof(bi));

						bi.bmiHeader.biSize			= sizeof(bi.bmiHeader);
						bi.bmiHeader.biBitCount		= 32;
						bi.bmiHeader.biWidth		= vclWnd->compList->clientRect.right;
						bi.bmiHeader.biHeight		= -vclWnd->compList->clientRect.bottom;
						bi.bmiHeader.biCompression	= BI_RGB;
						bi.bmiHeader.biPlanes		= 1;

						vclWnd->hMemBmp = CreateDIBSection(vclWnd->compList->memdc, &bi, DIB_RGB_COLORS, (void**)&(vclWnd->compList->hiddenBuffer), NULL, 0);
        
						SelectObject(vclWnd->compList->memdc, vclWnd->hMemBmp);
						SetBkMode(vclWnd->compList->memdc, TRANSPARENT);

						SendMessage(hWnd, WM_PAINT, NULL, NULL);
						
						break;
					}
				case WM_CREATE:
					{
						//***		Window handle will be written in extra window memory after it will be created
						//***		so we used incoming parameter from the CreateWindow function call to pass it
						
						vclWnd = *((VCLWindow**) lParam);

						GetClientRect(hWnd, &vclWnd->compList->clientRect); 

						vclWnd->hdc						= GetDC(hWnd);
						vclWnd->compList->memdc			= CreateCompatibleDC(vclWnd->hdc);
						
						BITMAPINFO	bi;

						memset(&bi, 0, sizeof(bi));

						bi.bmiHeader.biSize			= sizeof(bi.bmiHeader);
						bi.bmiHeader.biBitCount		= 32;
						bi.bmiHeader.biWidth		= vclWnd->compList->clientRect.right;
						bi.bmiHeader.biHeight		= -vclWnd->compList->clientRect.bottom;
						bi.bmiHeader.biCompression	= BI_RGB;
						bi.bmiHeader.biPlanes		= 1;

						vclWnd->hMemBmp = CreateDIBSection(vclWnd->compList->memdc, &bi, DIB_RGB_COLORS, (void**)&(vclWnd->compList->hiddenBuffer), NULL, 0);
        
						SelectObject(vclWnd->compList->memdc, vclWnd->hMemBmp);
						SetBkMode(vclWnd->compList->memdc, TRANSPARENT);

						break;
					}
				case WM_DESTROY:
					{
						WaitForSingleObject(vclWnd->windowMutex, INFINITE);

						vclWnd->closed = VCLCLOSING;	

						DeleteDC(vclWnd->compList->memdc);
						DeleteObject(vclWnd->hMemBmp);
						ReleaseDC(hWnd, vclWnd->hdc);
						
						PostQuitMessage(0);
						
						CloseHandle(vclWnd->renderMutex);

						ReleaseMutex(vclWnd->windowMutex);

						return 0;
					}
			}

		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}

//***			VCLButton

VCLButton::VCLButton(VCL_BUTTON_CREATE_PARAMS*	inParams) : VCLComponent	(
																				inParams->parentWindow,
																				inParams->parentComponent,
																				inParams->renderList,
																				inParams->btnX,
																				inParams->btnY,
																				inParams->btnWidth,
																				inParams->btnHeight,
																				inParams->btnVisible
																			)
	{
		borderWidth	= inParams->btnBorderWidth;

		unsigned int doubleWdh = borderWidth * 2;

		if(width < doubleWdh)
			width	= doubleWdh;

		if(height < doubleWdh)
			height	= doubleWdh;

		bkColor		= inParams->btnBkColor;
		brdColor	= inParams->btnBrdColor;
		capColor	= inParams->btnCapColor;
		bkSelColor	= inParams->btnBkSelColor;
		brdSelColor	= inParams->btnBrdSelColor;
		capSelColor	= inParams->btnCapSelColor;

		caption		= inParams->btnCaption;

		capX		= inParams->capX + borderWidth;
		capY		= inParams->capY + borderWidth;

		pressed		= 0;

		rebuildSprite(VCLALLOC);

		if(visible)
			{
				renderList->addToEnd(this);

				update();
			}
	}

VCLButton::~VCLButton()
	{
		if(caption != VCLNULL)
			delete caption;
	}

void VCLButton::setCaptionX(int inCapX)
	{
		capX = inCapX + borderWidth;
	}

void VCLButton::setCaptionY(int inCapY)
	{
		capY = inCapY  + borderWidth;
	}

int	VCLButton::getCaptionX()
	{
		return capX - borderWidth;
	}

int	VCLButton::getCaptionY()
	{
		return capY - borderWidth;
	}

void VCLButton::setBorderWidth(unsigned int newBrdWidth)
	{
		int newOffset = newBrdWidth - borderWidth;

		capX		+= newOffset;
		capY		+= newOffset;

		width		+= newOffset * 2;
		height		+= newOffset * 2;

		borderWidth = newBrdWidth;

		rebuildSprite(VCLREALLOC);
	}

unsigned int VCLButton::getBorderWidth()
	{
		return borderWidth;
	}

char VCLButton::react(unsigned int msg, unsigned int wParam, unsigned int lParam)
	{
		char res = VCLNULL;

		switch(msg)
			{
				case VCL_LBTN_DOWN:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(xPos >= x && xPos < x + (int)width && yPos >= y && yPos < y + (int)height)
							{
								//***	pushed render

								pressed = VCLACCEPT;

								rebuildSprite(VCLNOALLOC);
								
								if(renderList->getSelectedComp() != VCLNULL)
									renderList->getSelectedComp()->unfocus();

								focus();

								update();
								
								//***	

								if(MouseLDownHandle != VCLNULL)
									MouseLDownHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_LBTN_UP:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(renderList->getSelectedComp() == this)
							{
								//***	released render
								
								pressed = VCLNULL;

								rebuildSprite(VCLNOALLOC);
								update();

								//***	

								if(MouseLUpHandle != VCLNULL)
									MouseLUpHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_RBTN_DOWN:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(xPos >= x && xPos < x + (int)width && yPos >= y && yPos < y + (int)height)
							{
								if(MouseRDownHandle != VCLNULL)
									MouseRDownHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_RBTN_UP:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(renderList->getSelectedComp() == this)
							{
								if(MouseRUpHandle != VCLNULL)
									MouseRUpHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_MBTN_DOWN:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(xPos >= x && xPos < x + (int)width && yPos >= y && yPos < y + (int)height)
							{
								if(MouseMDownHandle != VCLNULL)
									MouseMDownHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_MBTN_UP:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(renderList->getSelectedComp() == this)
							{	
								if(MouseMUpHandle != VCLNULL)
									MouseMUpHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_MMOVE:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(xPos >= x && xPos < x + (int)width && yPos >= y && yPos < y + (int)height)
							{
								if(MouseMoveHandle != VCLNULL)
									MouseMoveHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_SCROLL:
					{
						if(((POINT*)lParam)->x >= x && ((POINT*)lParam)->x < x + (int)width && ((POINT*)lParam)->y >= y && ((POINT*)lParam)->y < y + (int)height)
							{
								if((short)(wParam >> 16) < 0 && MouseScrollUpHandle != VCLNULL)
									MouseScrollUpHandle(this, (long)wParam, (long)lParam);
								else if(MouseScrollDownHandle != VCLNULL)
									MouseScrollDownHandle(this, (long)wParam, (long)lParam);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_KEY_DOWN:
					{
						if(renderList->getSelectedComp() == this)
							{
								if(wParam == VK_TAB)
									{
										unfocus();

										if(next != VCLNULL)
											next->focus();
										else
											(renderList->getFirstInList())->focus();											
									}

								if(KeyDownHandle != VCLNULL)
									KeyDownHandle(this, wParam);
 
								res = VCLACCEPT;
							}

						break;
					}
				case VCL_KEY_UP:
					{
						if(renderList->getSelectedComp() == this)
							{
								if(KeyUpHandle != VCLNULL)
									KeyUpHandle(this, wParam);

								res = VCLACCEPT;
							}
					}
			}

		return res;
	}

void VCLButton::rebuildSprite(char realloc)
	{
		VCLComponent::rebuildSprite(realloc);
		
		VCL_TYPED_HANDLE col;

		if(pressed == VCLACCEPT)
			col = bkSelColor;
		else
			col = bkColor;

		const unsigned int	clientStep		= borderWidth * 2;	//const

		unsigned int		clientWidth		= width		- clientStep;
		unsigned int		clientBottom	= (height	- borderWidth) * width;

		unsigned int		clientPos		= borderWidth * width + borderWidth;

		if(col.type == VCL_HT_RGBA_COLOR)
			{
				while(clientPos < clientBottom)
					{
						for(unsigned int xPos = 0; xPos < clientWidth; xPos++)
							{
								sprite[clientPos] = *((RGBAPix*)(col.handle));
								clientPos++;
							}

						clientPos += clientStep;
					}
			}
		else if(col.type == VCL_HT_GENERIC_COLOR)
			{
				while(clientPos < clientBottom)
					{
						for(unsigned int xPos = 0; xPos < clientWidth; xPos++)
							{
								sprite[clientPos] = ((VCLGenericColor*)(col.handle))->getColor	(
																									clientPos % width,
																									clientPos / width,
																									width,
																									height
																								);
								clientPos++;
							}

						clientPos += clientStep;
					}
			}

		//getting border color

		if(pressed == VCLACCEPT)
			{
				col.type = brdSelColor.type;

				if(col.type == VCL_HT_RGBA_COLOR)
					{
						col.handle = malloc(sizeof(RGBAPix));
						memcpy(col.handle, brdSelColor.handle, sizeof(RGBAPix));
					}
				else if(col.type == VCL_HT_GENERIC_COLOR)
					col.handle = brdSelColor.handle;
			}
		else
			{
				col.type = brdColor.type;

				if(col.type == VCL_HT_RGBA_COLOR)
					{
						col.handle = malloc(sizeof(RGBAPix));
						memcpy(col.handle, brdColor.handle, sizeof(RGBAPix));
					}
				else if(col.type == VCL_HT_GENERIC_COLOR)
					col.handle = brdColor.handle;
			}	

		if(col.type == VCL_HT_RGBA_COLOR)
			{
				clientBottom		= height - clientStep;			// now its height in pix

				clientPos			= borderWidth * width + borderWidth;
		
				unsigned int endPos = clientPos + (clientWidth + clientStep) * (clientBottom - 1);

				for(unsigned int iter = borderWidth; iter > 0; iter--)
					{
						clientPos	-= width + 1;
						endPos		+= width - 1;

						clientWidth += 2;

						((RGBAPix*)(col.handle))->a = (((RGBAPix*)(brdColor.handle))->a * iter) / borderWidth;

						for(unsigned int index = 0; index < clientWidth; index++)
							{
								sprite[clientPos	+ index] = *((RGBAPix*)(col.handle));
								sprite[endPos		+ index] = *((RGBAPix*)(col.handle));
							}
					}

				clientWidth			= width			- clientStep;

				clientPos			= borderWidth	* width + borderWidth + width;
				endPos				= clientPos		+ clientWidth - 1;
		
				//((RGBAPix*)(col.handle))->a			= ((RGBAPix*)(brdColor.handle))->a;

				for(unsigned int iter = borderWidth; iter > 0; iter--)
					{
						clientPos	-= width + 1;
						endPos		-= width - 1;

						((RGBAPix*)(col.handle))->a = (((RGBAPix*)(brdColor.handle))->a * iter) / borderWidth;

						for(unsigned int index = 0; index < clientBottom; index++)
							{
								//maybe optimize here : not (index * width), but += width....

								sprite[clientPos	+ index * width] = *((RGBAPix*)(col.handle));
								sprite[endPos		+ index * width] = *((RGBAPix*)(col.handle));
							}

						clientBottom += 2;
					}

				free(col.handle);
			}
		else if(col.type == VCL_HT_GENERIC_COLOR)
			{
				clientBottom		= height - clientStep;			// now its height in pix

				clientPos			= borderWidth * width + borderWidth;
		
				unsigned int endPos = clientPos + (clientWidth + clientStep) * (clientBottom - 1);

				for(unsigned int iter = borderWidth; iter > 0; iter--)
					{
						clientPos	-= width + 1;
						endPos		+= width - 1;

						clientWidth += 2;

						//((RGBAPix*)(col.handle))->a = (((RGBAPix*)(brdColor.handle))->a * iter) / borderWidth;

						for(unsigned int index = 0; index < clientWidth; index++)
							{
								sprite[clientPos	+ index] = ((VCLGenericColor*)(col.handle))->getColor	(
																												(clientPos + index) % width,
																												(clientPos + index) / width,
																												width,
																												height
																											);

								sprite[endPos		+ index] = ((VCLGenericColor*)(col.handle))->getColor	(
																												(endPos + index) % width,
																												(endPos + index) / width,
																												width,
																												height
																											);
							}
					}

				clientWidth			= width			- clientStep;

				clientPos			= borderWidth	* width + borderWidth + width;
				endPos				= clientPos		+ clientWidth - 1;
		
				//((RGBAPix*)(col.handle))->a			= ((RGBAPix*)(brdColor.handle))->a;

				for(unsigned int iter = borderWidth; iter > 0; iter--)
					{
						clientPos	-= width + 1;
						endPos		-= width - 1;

						//((RGBAPix*)(col.handle))->a = (((RGBAPix*)(brdColor.handle))->a * iter) / borderWidth;

						for(unsigned int index = 0; index < clientBottom; index++)
							{
								//maybe optimize here : not (index * width), but += width....

								sprite[clientPos	+ index * width] = ((VCLGenericColor*)(col.handle))->getColor	(
																														(clientPos + index * width) % width,
																														(clientPos + index * width) / width,
																														width,
																														height
																													);

								sprite[endPos		+ index * width] = ((VCLGenericColor*)(col.handle))->getColor	(
																														(endPos + index * width) % width,
																														(endPos + index * width) / width,
																														width,
																														height
																													);
							}

						clientBottom += 2;
					}
			}

		if(pressed == VCLACCEPT)
			col = capSelColor;
		else
			col = capColor;

		if(parentWindow != VCLNULL && caption != VCLNULL)
			caption->render(parentWindow, sprite, capX, capY, width, height, col, width - clientStep, height - clientStep, 0);
	}

//***			VCLTextField

VCLTextField::VCLTextField(VCL_TXTFIELD_CREATE_PARAMS* inParams) : VCLComponent	(
																					inParams->parentWindow,
																					inParams->parentComponent,
																					inParams->renderList,
																					inParams->fldX,
																					inParams->fldY,
																					inParams->fldWidth,
																					inParams->fldHeight,
																					inParams->fldVisible
																				)
	{
		caption		= inParams->fldCaption;
		
		bkColor		= inParams->fldBkColor;
		brdColor	= inParams->fldBrdColor;
		capColor	= inParams->fldCapColor;
		bkSelColor	= inParams->fldBkSelColor;
		brdSelColor	= inParams->fldBrdSelColor;
		capSelColor	= inParams->fldCapSelColor;

		borderWidth	= inParams->fldBorderWidth;


		isThreadExist = VCLNULL;

		unsigned int doubleWdh = borderWidth * 2;

		if(width < doubleWdh)
			width	= doubleWdh;

		if(height < doubleWdh)
			height	= doubleWdh;

		CharTypedHandle = VCLNULL;

		rebuildSprite(VCLALLOC);

		if(visible)
			{
				renderList->addToEnd(this);
				update();
			}
	}

VCLTextField::~VCLTextField()
	{
		if(caption != VCLNULL)
			delete caption;

		CloseHandle(caretRenderMutex);
	}

void VCLTextField::setBorderWidth(unsigned int newBrdWidth)
	{
		int newOffset	= newBrdWidth - borderWidth;

		width			+= newOffset * 2;
		height			+= newOffset * 2;

		borderWidth		= newBrdWidth;

		rebuildSprite(VCLREALLOC);
	}

unsigned int VCLTextField::getBorderWidth()
	{
		return borderWidth;
	}

DWORD VCLTextField::newThreadMethod(VCL_TXTFIELD_THREAD_METHOD_PARAM* mParam)
	{
		VCLTextField*				pObject = mParam->object;
		VCL_TXTFIELD_THREAD_METHOD	pMethod = mParam->method;
		LPVOID						pParam	= mParam->param;

		DWORD						res		= (pObject->*pMethod)(pParam);

		delete mParam;
		return res;
	}

char VCLTextField::react(unsigned int msg, unsigned int wParam, unsigned int lParam)
	{
		char res = VCLNULL;

		switch(msg)
			{
				case VCL_LBTN_DOWN:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(xPos >= x && xPos < x + (int)width && yPos >= y && yPos < y + (int)height)
							{
								//***	pushed render
								
								if(renderList->getSelectedComp() != VCLNULL)
									renderList->getSelectedComp()->unfocus();

								focus();

								rebuildSprite(VCLNOALLOC);

								update();

								//***	

								if(MouseLDownHandle != VCLNULL)
									MouseLDownHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_LBTN_UP:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(renderList->getSelectedComp() == this)
							{
								if(MouseLUpHandle != VCLNULL)
									MouseLUpHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_RBTN_DOWN:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(xPos >= x && xPos < x + (int)width && yPos >= y && yPos < y + (int)height)
							{
								if(MouseRDownHandle != VCLNULL)
									MouseRDownHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_RBTN_UP:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(renderList->getSelectedComp() == this)
							{
								if(MouseRUpHandle != VCLNULL)
									MouseRUpHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_MBTN_DOWN:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(xPos >= x && xPos < x + (int)width && yPos >= y && yPos < y + (int)height)
							{
								if(MouseMDownHandle != VCLNULL)
									MouseMDownHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_MBTN_UP:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(renderList->getSelectedComp() == this)
							{	
								if(MouseMUpHandle != VCLNULL)
									MouseMUpHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_MMOVE:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(xPos >= x && xPos < x + (int)width && yPos >= y && yPos < y + (int)height)
							{
								if(MouseMoveHandle != VCLNULL)
									MouseMoveHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_SCROLL:
					{
						if(((POINT*)lParam)->x >= x && ((POINT*)lParam)->x < x + (int)width && ((POINT*)lParam)->y >= y && ((POINT*)lParam)->y < y + (int)height)
							{
								if((short)(wParam >> 16) < 0 && MouseScrollUpHandle != VCLNULL)
									MouseScrollUpHandle(this, (long)wParam, (long)lParam);
								else if(MouseScrollDownHandle != VCLNULL)
									MouseScrollDownHandle(this, (long)wParam, (long)lParam);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_KEY_DOWN:
					{
						if(renderList->getSelectedComp() == this)
							{
								switch(wParam)
									{
										case VK_DELETE:
											{
												caption->delChar(caretPos);
												rebuildSprite(VCLNOALLOC);
												update();
											
												break;
											}
										case VK_LEFT:
											{
												if(caretPos > 0)
													caretPos--;

												if((unsigned short)lParam == 1)
													{
														WaitForSingleObject(caretRenderMutex, INFINITE);
														
														caretOn			= VCLACCEPT;
														caretBlinkOn	= VCLNULL;

														ReleaseMutex(caretRenderMutex);
													}

												rebuildSprite(VCLNOALLOC);
												update();

												break;
											}
										case VK_RIGHT:
											{
												if(caretPos < caption->getLength())
													caretPos++;

												if((unsigned short)lParam == 1)
													{
														WaitForSingleObject(caretRenderMutex, INFINITE);
														
														caretOn			= VCLACCEPT;
														caretBlinkOn	= VCLNULL;

														ReleaseMutex(caretRenderMutex);
													}

												rebuildSprite(VCLNOALLOC);
												update();

												break;
											}
										case VK_TAB:
											{
												unfocus();

												if(next != VCLNULL)
													next->focus();
												else
													(renderList->getFirstInList())->focus();

												update();										
											}
									}

								if(KeyDownHandle != VCLNULL)
									KeyDownHandle(this, wParam);
 
								res = VCLACCEPT;
							}

						break;
					}
				case VCL_CHAR:
					{
						if(renderList->getSelectedComp() == this)
							{
								switch (wParam)
									{
										case 8:	//backspace 
											{
												if(caretPos > 0)
													{
														caption->delChar(caretPos - 1);
														caretPos--;
													}

												rebuildSprite(VCLNOALLOC);

												break;
											}
										case 27: //esc
											{
												unfocus(); 

												break;
											}
										case 13: //enter
											{
												unfocus();

												break;
											}
										default:
											{
												caption->insChar(caretPos, wParam);
												caretPos++;

												rebuildSprite(VCLNOALLOC);
											}
									}

								update();

								if(CharTypedHandle != VCLNULL)
									CharTypedHandle(this, wParam);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_KEY_UP:
					{
						if(renderList->getSelectedComp() == this)
							{
								if(wParam == VK_LEFT || wParam == VK_RIGHT)
									{
										WaitForSingleObject(caretRenderMutex, INFINITE);

										caretBlinkOn	= VCLACCEPT;

										ReleaseMutex(caretRenderMutex);
									}

								if(KeyUpHandle != VCLNULL)
									KeyUpHandle(this, wParam);

								res = VCLACCEPT;
							}
					}
			}

		return res;
	}

unsigned int VCLTextField::processCaret(LPVOID param)
	{
		while(true)
			{
				Sleep(500);

				WaitForSingleObject(caretRenderMutex, INFINITE);

				if(renderList->getSelectedComp() != this)
					{
						ReleaseMutex(caretRenderMutex);
						break;
					}

				if(caretBlinkOn == VCLACCEPT)
					{
						if(caretOn == VCLACCEPT)
							caretOn = VCLNULL;
						else
							caretOn = VCLACCEPT;

						rebuildSprite(VCLNOALLOC);

						update();
					}

				ReleaseMutex(caretRenderMutex);
			}

		isThreadExist = VCLNULL;

		return id; //DEBUG
	}

void VCLTextField::focus()
	{
		WaitForSingleObject(caretRenderMutex, INFINITE);

		if(renderList->getSelectedComp() != this && isThreadExist == VCLNULL)
			{
				VCLComponent::focus();

				caretOn			= VCLACCEPT;
				caretBlinkOn	= VCLACCEPT;

				VCL_TXTFIELD_THREAD_METHOD_PARAM* mParam = new VCL_TXTFIELD_THREAD_METHOD_PARAM;

				mParam->method	= &VCLTextField::processCaret; 
				mParam->object	= this;
				mParam->param	= VCLNULL;

				isThreadExist	= VCLACCEPT;

				CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)newThreadMethod, mParam, 0, NULL);

				rebuildSprite(VCLNOALLOC);
			}

		ReleaseMutex(caretRenderMutex);
	}

void VCLTextField::unfocus()
	{
		WaitForSingleObject(caretRenderMutex, INFINITE);

		VCLComponent::unfocus();

		caretOn			= VCLNULL;
		caretPos		= 0;

		rebuildSprite(VCLNOALLOC);

		ReleaseMutex(caretRenderMutex);
	}

void VCLTextField::rebuildSprite(char realloc)
	{
		VCLComponent::rebuildSprite(realloc);
		
		VCL_TYPED_HANDLE col;

		if(renderList->getSelectedComp() == this)
			col = bkSelColor;
		else
			col = bkColor;

		unsigned int clientStep		= borderWidth * 2;

		unsigned int clientWidth	= width		- clientStep;
		unsigned int clientBottom	= (height	- borderWidth) * width;

		unsigned int clientPos		= borderWidth * width + borderWidth;

		if(col.type == VCL_HT_RGBA_COLOR)
			{
				while(clientPos < clientBottom)
					{
						for(unsigned int xPos = 0; xPos < clientWidth; xPos++)
							{
								sprite[clientPos] = *((RGBAPix*)(col.handle));
								clientPos++;
							}

						clientPos += clientStep;
					}
			}
		else if(col.type == VCL_HT_GENERIC_COLOR)
			{
				while(clientPos < clientBottom)
					{
						for(unsigned int xPos = 0; xPos < clientWidth; xPos++)
							{
								sprite[clientPos] = ((VCLGenericColor*)(col.handle))->getColor	(
																									clientPos % width,
																									clientPos / width,
																									width,
																									height
																								);
								clientPos++;
							}

						clientPos += clientStep;
					}
			}

		//getting border color

		if(renderList->getSelectedComp() == this)
			{
				col.type = brdSelColor.type;

				if(col.type == VCL_HT_RGBA_COLOR)
					{
						col.handle = malloc(sizeof(RGBAPix));
						memcpy(col.handle, brdSelColor.handle, sizeof(RGBAPix));
					}
				else if(col.type == VCL_HT_GENERIC_COLOR)
					col.handle = brdSelColor.handle;
			}
		else
			{
				col.type = brdColor.type;

				if(col.type == VCL_HT_RGBA_COLOR)
					{
						col.handle = malloc(sizeof(RGBAPix));
						memcpy(col.handle, brdColor.handle, sizeof(RGBAPix));
					}
				else if(col.type == VCL_HT_GENERIC_COLOR)
					col.handle = brdColor.handle;
			}

		clientBottom		= height - clientStep;			// now its height in pix

		clientPos			= borderWidth * width + borderWidth;
		
		unsigned int endPos = clientPos + (clientWidth + clientStep) * (clientBottom - 1);

		if(col.type == VCL_HT_RGBA_COLOR)
			{
				for(unsigned int iter = 0; iter < borderWidth; iter++)
					{
						clientPos	-= width + 1;
						endPos		+= width - 1;

						clientWidth += 2;

						((RGBAPix*)(col.handle))->a = (((RGBAPix*)(brdColor.handle))->a * iter) / borderWidth;

						for(unsigned int index = 0; index < clientWidth; index++)
							{
								sprite[clientPos	+ index] = *((RGBAPix*)(col.handle));
								sprite[endPos		+ index] = *((RGBAPix*)(col.handle));
							}
					}

				clientWidth			= width			- clientStep;

				clientPos			= borderWidth	* width + borderWidth + width;
				endPos				= clientPos		+ clientWidth - 1;
		
				((RGBAPix*)(col.handle))->a			= ((RGBAPix*)(brdColor.handle))->a;

				for(unsigned int iter = 0; iter < borderWidth; iter++)
					{
						clientPos	-= width + 1;
						endPos		-= width - 1;

						((RGBAPix*)(col.handle))->a = (((RGBAPix*)(brdColor.handle))->a * iter) / borderWidth;

						for(unsigned int index = 0; index < clientBottom; index++)
							{
								//maybe optimize here : not (index * width), but += width....

								sprite[clientPos	+ index * width] = *((RGBAPix*)(col.handle));
								sprite[endPos		+ index * width] = *((RGBAPix*)(col.handle));
							}

						clientBottom += 2;
					}

				free(col.handle);
			}
		else if(col.type == VCL_HT_GENERIC_COLOR)
			{
				for(unsigned int iter = 0; iter < borderWidth; iter++)
					{
						clientPos	-= width + 1;
						endPos		+= width - 1;

						clientWidth += 2;

						//((RGBAPix*)(col.handle))->a = (((RGBAPix*)(brdColor.handle))->a * iter) / borderWidth;

						for(unsigned int index = 0; index < clientWidth; index++)
							{
								sprite[clientPos	+ index] = ((VCLGenericColor*)(col.handle))->getColor	(
																												(clientPos + index) % width,
																												(clientPos + index) / width,
																												width,
																												height
																											);
								sprite[endPos		+ index] = ((VCLGenericColor*)(col.handle))->getColor	(
																												(endPos + index) % width,
																												(endPos + index) / width,
																												width,
																												height
																											);
							}
					}

				clientWidth			= width			- clientStep;

				clientPos			= borderWidth	* width + borderWidth + width;
				endPos				= clientPos		+ clientWidth - 1;
		
				//((RGBAPix*)(col.handle))->a			= ((RGBAPix*)(brdColor.handle))->a;

				for(unsigned int iter = 0; iter < borderWidth; iter++)
					{
						clientPos	-= width + 1;
						endPos		-= width - 1;

						//((RGBAPix*)(col.handle))->a = (((RGBAPix*)(brdColor.handle))->a * iter) / borderWidth;

						for(unsigned int index = 0; index < clientBottom; index++)
							{
								//maybe optimize here : not (index * width), but += width....

								sprite[clientPos	+ index * width] = ((VCLGenericColor*)(col.handle))->getColor	(
																														(clientPos + index * width) % width,
																														(clientPos + index * width) / width,
																														width,
																														height
																													);

								sprite[endPos		+ index * width] = ((VCLGenericColor*)(col.handle))->getColor	(
																														(endPos + index * width) % width,
																														(endPos + index * width) / width,
																														width,
																														height
																													);
							}

						clientBottom += 2;
					}
			}

		if(renderList->getSelectedComp() == this)
			col = capSelColor;
		else
			col = capColor;

		SIZE carPos;
		memset(&carPos, 0, sizeof(SIZE));

		if(parentWindow != VCLNULL && caption != VCLNULL)
			carPos = caption->render(parentWindow, sprite, borderWidth, borderWidth, width, height, col, width - clientStep, height - clientStep, caretPos);

		if(caretOn == VCLACCEPT)
			{
				carPos.cy		+= borderWidth;
				carPos.cx		+= borderWidth;

				if(carPos.cx == width - borderWidth)
					carPos.cx -= 2;			
	
				clientStep		= width - 2;

				clientWidth		= 2;
				clientBottom	= carPos.cy * width;

				clientPos		= borderWidth * width + carPos.cx;

				while(clientPos < clientBottom)
					{
						for(unsigned int xPos = 0; xPos < clientWidth; xPos++)
							{
								sprite[clientPos].b = 255 - sprite[clientPos].b;
								sprite[clientPos].g = 255 - sprite[clientPos].g;
								sprite[clientPos].r = 255 - sprite[clientPos].r;
												/*= {
														(unsigned char)(255) - sprite[clientPos].b,
														(unsigned char)(255) - sprite[clientPos].g,
														(unsigned char)(255) - sprite[clientPos].r,
														sprite[clientPos].a
													};*/
								clientPos++;
							}

						clientPos += clientStep;
					}
			}
	}

//***		VCLOpenDialog

VCLOpenDialog::VCLOpenDialog(VCL_DIALOG_CREATE_PARAMS* inParams)
	{
		hwndOwner					= inParams->wndOwner;
		filters						= inParams->filters;
		defaultExt					= inParams->defaultExt;
		optionsEx					= inParams->optionsEx;
		options						= inParams->options;
		filterIndex					= inParams->filterIndex;
		filePath					= inParams->filePath;
		maxFilePath					= inParams->maxFilePath;
		initialDir					= inParams->initialDir;
		title						= inParams->title;
	}

bool VCLOpenDialog::execute()
	{
		OPENFILENAME	params;

		params.lStructSize			= sizeof(OPENFILENAME);
		params.hInstance			= NULL;
		params.lCustData			= NULL;
		params.lpfnHook				= NULL;
		params.lpTemplateName		= NULL;
		params.lpstrCustomFilter	= NULL;
		params.nMaxCustFilter		= 0;
		params.lpstrFileTitle		= NULL;
		params.nMaxFileTitle		= 0;

		params.hwndOwner			= hwndOwner;

		params.lpstrFile			= new wchar_t[maxFilePath];
		
		if(filePath)
			memcpy(params.lpstrFile, filePath, wcslen(filePath));
		else
			*params.lpstrFile		= 0;

		params.nMaxFile				= maxFilePath;
		params.lpstrInitialDir		= initialDir;
		params.lpstrTitle			= title;
		params.lpstrFilter			= filters;
		params.lpstrDefExt			= defaultExt;
		params.FlagsEx				= optionsEx;
		params.Flags				= options;
		params.nFilterIndex			= filterIndex;

		bool res					= GetOpenFileName(&params);

		if(filePath)
			delete filePath;

		filePath					= params.lpstrFile;

		return res;
	}

VCLSaveDialog::VCLSaveDialog(VCL_DIALOG_CREATE_PARAMS* inParams) : VCLOpenDialog(inParams)
	{}

bool VCLSaveDialog::execute()
	{
		OPENFILENAME	params;

		params.lStructSize			= sizeof(OPENFILENAME);
		params.hInstance			= NULL;
		params.lCustData			= NULL;
		params.lpfnHook				= NULL;
		params.lpTemplateName		= NULL;
		params.lpstrCustomFilter	= NULL;
		params.nMaxCustFilter		= 0;
		params.lpstrFileTitle		= NULL;
		params.nMaxFileTitle		= 0;

		params.hwndOwner			= hwndOwner;

		params.lpstrFile			= new wchar_t[maxFilePath];
		
		if(filePath)
			memcpy(params.lpstrFile, filePath, wcslen(filePath));
		else
			*params.lpstrFile		= 0;

		params.nMaxFile				= maxFilePath;
		params.lpstrInitialDir		= initialDir;
		params.lpstrTitle			= title;
		params.lpstrFilter			= filters;
		params.lpstrDefExt			= defaultExt;
		params.FlagsEx				= optionsEx;
		params.Flags				= options;
		params.nFilterIndex			= filterIndex;

		bool res					= GetSaveFileName(&params);

		if(filePath)
			delete filePath;

		filePath					= params.lpstrFile;

		return res;
	}

//***		VCLTextGrid

VCLTextGrid::VCLTextGrid(VCL_TXTGRID_CREATE_PARAMS* inParams) : VCLComponent	(
																					inParams->parentWindow,
																					inParams->parentComponent,
																					inParams->renderList,
																					inParams->grdX,
																					inParams->grdY,
																					inParams->grdWidth,
																					inParams->grdHeight,
																					inParams->grdVisible
																				)
	{
		rowCount				= inParams->grdRowCount;
		colCount				= inParams->grdColCount;
	
		bkColor					= inParams->grdBkColor;
		bkSelColor				= inParams->grdBkSelColor;
		capColor				= inParams->grdCapColor;
		capSelColor				= inParams->grdCapSelColor;
		grdColor				= inParams->grdGrdColor;
		bkMarkColor				= inParams->grdBkMarkColor;
		capMarkColor			= inParams->grdCapMarkColor;
		fixBkColor				= inParams->grdFixBkColor;
		fixCapColor				= inParams->grdFixCapColor;

		fixColCount				= inParams->grdFixColCount;
		fixRowCount				= inParams->grdFixRowCount;
		
		gridWidth				= inParams->grdGridWidth;

		firstRowRender			= fixRowCount;
		firstCellRender			= fixColCount;

		rowSelect				= VCLNULL;
		readOnly				= VCLNULL;

		rows					= VCLNULL;
		lastRow					= VCLNULL;
		widthList				= VCLNULL;
		heightList				= VCLNULL;

		selectedCell			= VCLNULL;

		CharTypedHandle			= VCLNULL;
		isThreadExist			= VCLNULL;
		caretBlinkOn			= VCLNULL;
		caretOn					= VCLNULL;

		VCL_SIZE_VALUE* currWdh = VCLNULL;
		VCL_SIZE_VALUE* currHgh = VCLNULL;

		for(unsigned int yindex = 0; yindex < rowCount; yindex++)
			{
				if(heightList != VCLNULL)
					{
						currHgh->next = new VCL_SIZE_VALUE;
						currHgh->next->value = inParams->grdInitialCellHeight;
						currHgh = currHgh->next;
					}
				else
					{
						currHgh = new VCL_SIZE_VALUE;
						currHgh->value = inParams->grdInitialCellHeight;
						heightList = currHgh;
					}

				if(rows != VCLNULL)
					{
						rows->prev = new VCL_TEXT_ROW;

						rows->prev->prev	= VCLNULL;
						rows->prev->next	= rows;

						rows				= rows->prev;

						rows->cell			= VCLNULL;
						rows->lastCell		= VCLNULL;

						/*
							cells create
						*/

						for(unsigned int xindex = 0; xindex < colCount; xindex++)
							{
								if(rows->cell == VCLNULL)
									{
										rows->cell			= new VCL_TEXT_CELL;
										rows->lastCell		= rows->cell;

										rows->cell->prev	= VCLNULL;
										rows->cell->next	= VCLNULL;
									}
								else
									{
										rows->cell->prev		= new VCL_TEXT_CELL;
										
										rows->cell->prev->next	= rows->cell;
										rows->cell->prev->prev	= VCLNULL;
										
										rows->cell				= rows->cell->prev;
									}

								rows->cell->caption		= new VCLString(L"");
								rows->cell->caretPos	= 0;
								rows->cell->color		= bkColor;
								rows->cell->selColor	= bkSelColor;
								rows->cell->txtColor	= capColor;
								rows->cell->txtSelColor	= capSelColor;
								rows->cell->marked		= VCLNULL;
							}
					}
				else
					{
						rows			= new VCL_TEXT_ROW;
						lastRow			= rows;
					
						rows->prev		= VCLNULL;
						rows->next		= VCLNULL;

						rows->cell		= VCLNULL;
						rows->lastCell	= VCLNULL;

						/*
							cells create
						*/

						for(unsigned int xindex = 0; xindex < colCount; xindex++)
							{
								if(rows->cell == VCLNULL)
									{
										rows->cell			= new VCL_TEXT_CELL;
										rows->lastCell		= rows->cell;

										rows->cell->prev	= VCLNULL;
										rows->cell->next	= VCLNULL;
									}
								else
									{
										rows->cell->prev		= new VCL_TEXT_CELL;
										
										rows->cell->prev->next	= rows->cell;
										rows->cell->prev->prev	= VCLNULL;
										
										rows->cell				= rows->cell->prev;
									}

								rows->cell->caption		= new VCLString(L"");
								rows->cell->caretPos	= 0;
								rows->cell->color		= bkColor;
								rows->cell->selColor	= bkSelColor;
								rows->cell->txtColor	= capColor;
								rows->cell->txtSelColor	= capSelColor;
								rows->cell->marked		= VCLNULL;
							}
					}
			}

		if(currHgh != VCLNULL)
			currHgh->next = VCLNULL;

		lastHeight = currHgh;

		for(unsigned int index = 0; index < colCount; index++)
			{
				if(widthList != VCLNULL)
					{
						currWdh->next = new VCL_SIZE_VALUE;
						currWdh->next->value = inParams->grdInitialCellWidth;
						currWdh = currWdh->next;
					}
				else
					{
						currWdh = new VCL_SIZE_VALUE;
						currWdh->value = inParams->grdInitialCellWidth;
						widthList = currWdh;
					}
			}

		if(currWdh != VCLNULL)
			currWdh->next = VCLNULL;

		lastWidth = currWdh;

		rebuildSprite(VCLALLOC);

		if(visible)
			{
				renderList->addToEnd(this);

				update();
			}
	}

VCLTextGrid::~VCLTextGrid()
	{
		VCL_SIZE_VALUE* tmp = VCLNULL;

		while(widthList != VCLNULL)
			{
				tmp = widthList;
				widthList = widthList->next;
				delete tmp;
			}

		while(heightList != VCLNULL)
			{
				tmp = heightList;
				heightList = heightList->next;
				delete tmp;
			}

		while(true)
			{
				while(true)
					{
						delete rows->cell->caption;

						if(rows->cell->next != VCLNULL)
							{
								rows->cell = rows->cell->next;
								delete rows->prev;
							}
						else
							{
								delete rows->cell;
								break;
							}
					}
				if(rows->next != VCLNULL)
					{
						rows = rows->next;
						delete rows->prev;
					}
				else
					{
						delete rows;
						break;
					}
			}

		CloseHandle(caretRenderMutex);
	}

unsigned int VCLTextGrid::getRowCount()
	{
		return rowCount;
	}

unsigned int VCLTextGrid::getColCount()
	{
		return colCount;
	}

void VCLTextGrid::rebuildSprite(char realloc)
	{
		VCLComponent::rebuildSprite(realloc);

		VCL_TEXT_CELL*		currCell;
		VCL_TEXT_ROW*		currRow		= rows;

		int					currX;
		int					currY		= gridWidth;
		unsigned int		currCellNum;
		unsigned int		currRowNum	= 0;

		VCL_TYPED_HANDLE	col;

		unsigned int		clientPos;
		unsigned int		clientStep;
		unsigned int		clientWidth;
		unsigned int		clientBottom;

		SIZE				carPos;

		VCL_SIZE_VALUE*		currHgh		= heightList;
		VCL_SIZE_VALUE*		currWdh;
		RGBAPix				tmpCol;

		//background

		clientPos						= 0;

		if(grdColor.type == VCL_HT_RGBA_COLOR)
			{
				while(clientPos < width * height)
					{	
						sprite[clientPos] = *((RGBAPix*)(grdColor.handle));

						clientPos++;
					}
			}
		else if(grdColor.type == VCL_HT_GENERIC_COLOR)
			{
				while(clientPos < width * height)
					{	
						sprite[clientPos] = ((VCLGenericColor*)(grdColor.handle))->getColor	(
																								clientPos % width,
																								clientPos / width,
																								width,
																								height
																							);

						clientPos++;
					}
			}

		//background
		
		if(currRow != VCLNULL && currY < height)
			{
				currRowNum = 0;
				
				while(currRow != VCLNULL && currRowNum < fixRowCount && currY < height)
					{
						currCell	= currRow->cell;
						currWdh		= widthList;

						currX		= gridWidth;

						if(currCell != VCLNULL && currX < width)
							{
								currCellNum = 0;

								while(currCell != VCLNULL && currCellNum < fixColCount && currX < width)
									{
										//col

										/*if(currCell->marked == VCL_SELECTED)
											col = currCell->selColor;*/
										if(currCell->marked == VCL_MARKED)
											col	= bkMarkColor;
										else
											col = fixBkColor;

										//rect

										clientPos = currY * width + currX;

										if(currX + currWdh->value > width)
											clientStep = currX;
										else
											clientStep = width - currWdh->value;

										if(col.type == VCL_HT_RGBA_COLOR)
											{
												for(unsigned int yindex = 0; yindex < currHgh->value && yindex + currY < height; yindex++)
													{
														for(unsigned int xindex = 0; xindex < currWdh->value && xindex + currX < width; xindex++)
															{
																OPT_SET_TRANSP_PIX	(
																						clientPos,
																						((RGBAPix*)(col.handle))->b,
																						((RGBAPix*)(col.handle))->g,
																						((RGBAPix*)(col.handle))->r,
																						((RGBAPix*)(col.handle))->a,
																						sprite
																					);

																//sprite[clientPos] = col; 

																clientPos++;
															}

														clientPos += clientStep;
													}
											}
										else if(col.type == VCL_HT_GENERIC_COLOR)
											{
												for(unsigned int yindex = 0; yindex < currHgh->value && yindex + currY < height; yindex++)
													{
														for(unsigned int xindex = 0; xindex < currWdh->value && xindex + currX < width; xindex++)
															{
																tmpCol = ((VCLGenericColor*)(col.handle))->getColor	(
																														clientPos % width,
																														clientPos / width,
																														width,
																														height
																													);

																OPT_SET_TRANSP_PIX	(
																						clientPos,
																						tmpCol.b,
																						tmpCol.g,
																						tmpCol.r,
																						tmpCol.a,
																						sprite
																					);

																//sprite[clientPos] = col; 

																clientPos++;
															}

														clientPos += clientStep;
													}
											}

										//col

										/*if(currCell->marked == VCL_SELECTED)
											col = currCell->txtSelColor;*/
										if(currCell->marked == VCL_MARKED)
											col		=	capMarkColor;
										else
											col = fixCapColor;

										//cap

										if(parentWindow != VCLNULL && currCell->caption != VCLNULL)
											{
												if(currX + currWdh->value < width)
													clientWidth = currWdh->value;
												else
													clientWidth = width - currX;

												if(currY + currHgh->value < height)
													clientBottom = currHgh->value;
												else
													clientBottom = height - currY;

												carPos = currCell->caption->render(parentWindow, sprite, currX, currY, width, height, col, clientWidth, clientBottom, currCell->caretPos);
											}

										//caret

										if(currCell == selectedCell && caretOn == VCLACCEPT)
											{
												carPos.cx		+= currX;
												carPos.cy		+= currY;

												if(carPos.cx == currX + currWdh->value)
													carPos.cx -= 2;

												clientStep		= width - 2;

												clientWidth		= 2;
												clientBottom	= carPos.cy * width;

												clientPos		= currY * width + carPos.cx;

												while(clientPos < clientBottom)
													{
														for(unsigned int xPos = 0; xPos < clientWidth; xPos++)
															{
																sprite[clientPos].b = 255 - sprite[clientPos].b;
																sprite[clientPos].g = 255 - sprite[clientPos].g;
																sprite[clientPos].r = 255 - sprite[clientPos].r;
																					/*
																					{
																						255 - sprite[clientPos].b,
																						255 - sprite[clientPos].g,
																						255 - sprite[clientPos].r,
																						sprite[clientPos].a
																					};
																					*/
																clientPos++;
															}

														clientPos += clientStep;
													}
											}

										//caret

										currX		+=	currWdh->value + gridWidth;
										currCellNum	++;

										currCell	=	currCell->next;
										currWdh		=	currWdh->next;
									}
						
								while(currCell != VCLNULL && currCellNum < firstCellRender && currX < width)
									{
										//currX		+=	currWdh->value + gridWidth;
										currCellNum	++;

										currCell	=	currCell->next;
										currWdh		=	currWdh->next;
									}

								while(currCell != VCLNULL && currX < width)
									{
										//col

										/*if(currCell->marked == VCL_SELECTED)
											col = currCell->selColor;*/
										if(currCell->marked == VCL_MARKED)
											col	= bkMarkColor;
										else
											col = fixBkColor;

										//rect

										clientPos = currY * width + currX;

										if(currX + currWdh->value > width)
											clientStep = currX;
										else
											clientStep = width - currWdh->value;

										if(col.type == VCL_HT_RGBA_COLOR)
											{
												for(unsigned int yindex = 0; yindex < currHgh->value && yindex + currY < height; yindex++)
													{
														for(unsigned int xindex = 0; xindex < currWdh->value && xindex + currX < width; xindex++)
															{
																OPT_SET_TRANSP_PIX	(
																						clientPos,
																						((RGBAPix*)(col.handle))->b,
																						((RGBAPix*)(col.handle))->g,
																						((RGBAPix*)(col.handle))->r,
																						((RGBAPix*)(col.handle))->a,
																						sprite
																					);

																//sprite[clientPos] = col;

																clientPos++;
															}

														clientPos += clientStep;
													}
											}
										else if(col.type == VCL_HT_GENERIC_COLOR)
											{
												for(unsigned int yindex = 0; yindex < currHgh->value && yindex + currY < height; yindex++)
													{
														for(unsigned int xindex = 0; xindex < currWdh->value && xindex + currX < width; xindex++)
															{
																tmpCol = ((VCLGenericColor*)(col.handle))->getColor(clientPos % width, clientPos / width, width, height);

																OPT_SET_TRANSP_PIX	(
																						clientPos,
																						tmpCol.b,
																						tmpCol.g,
																						tmpCol.r,
																						tmpCol.a,
																						sprite
																					);

																//sprite[clientPos] = col;

																clientPos++;
															}

														clientPos += clientStep;
													}
											}

										//col

										/*if(currCell->marked == VCL_SELECTED)
											col = currCell->txtSelColor;*/
										if(currCell->marked == VCL_MARKED)
											col	= capMarkColor;
										else
											col = fixCapColor;

										//cap

										if(parentWindow != VCLNULL && currCell->caption != VCLNULL)
											{
												if(currX + currWdh->value < width)
													clientWidth = currWdh->value;
												else
													clientWidth = width - currX;

												if(currY + currHgh->value < height)
													clientBottom = currHgh->value;
												else
													clientBottom = height - currY;

												carPos = currCell->caption->render(parentWindow, sprite, currX, currY, width, height, col, clientWidth, clientBottom, currCell->caretPos);
											}

										//caret

										if(currCell == selectedCell && caretOn == VCLACCEPT)
											{
												carPos.cx		+= currX;
												carPos.cy		+= currY;

												if(carPos.cx == currX + currWdh->value)
													carPos.cx -= 2;

												clientStep		= width - 2;

												clientWidth		= 2;
												clientBottom	= carPos.cy * width;

												clientPos		= currY * width + carPos.cx;

												while(clientPos < clientBottom)
													{
														for(unsigned int xPos = 0; xPos < clientWidth; xPos++)
															{
																sprite[clientPos].b = 255 - sprite[clientPos].b;
																sprite[clientPos].g = 255 - sprite[clientPos].g;
																sprite[clientPos].r = 255 - sprite[clientPos].r;

																					/*{
																						255 - sprite[clientPos].b,
																						255 - sprite[clientPos].g,
																						255 - sprite[clientPos].r,
																						sprite[clientPos].a
																					};*/
																clientPos++;
															}

														clientPos += clientStep;
													}
											}

										//caret

										currX		+=	currWdh->value + gridWidth;
										currCell	=	currCell->next;
										currWdh		=	currWdh->next;
									}
							}

						currY += currHgh->value + gridWidth;
						currRowNum++;

						currRow = currRow->next;
						currHgh	= currHgh->next;
					}

				while(currRow != VCLNULL && currRowNum < firstRowRender && currY < height)
					{
						//currY += currHgh->value + gridWidth;
						currRowNum++;

						currRow = currRow->next;
						currHgh	= currHgh->next;
					}

				while(currRow != VCLNULL && currY < height)
					{
						currCell	= currRow->cell;
						currWdh		= widthList;

						currX		= gridWidth;

						if(currCell != VCLNULL && currX < width)
							{
								currCellNum = 0;

								while(currCell != VCLNULL && currCellNum < fixColCount && currX < width)
									{
										//col

										/*if(currCell->marked == VCL_SELECTED)
											col = currCell->selColor;*/
										if(currCell->marked == VCL_MARKED)
											col	= bkMarkColor;
										else
											col = fixBkColor;

										//rect

										clientPos = currY * width + currX;

										if(currX + currWdh->value > width)
											clientStep = currX;
										else
											clientStep = width - currWdh->value;

										if(col.type == VCL_HT_RGBA_COLOR)
											{
												for(unsigned int yindex = 0; yindex < currHgh->value && yindex + currY < height; yindex++)
													{
														for(unsigned int xindex = 0; xindex < currWdh->value && xindex + currX < width; xindex++)
															{
																OPT_SET_TRANSP_PIX	(
																						clientPos,
																						((RGBAPix*)(col.handle))->b,
																						((RGBAPix*)(col.handle))->g,
																						((RGBAPix*)(col.handle))->r,
																						((RGBAPix*)(col.handle))->a,
																						sprite
																					);

																//sprite[clientPos] = col;

																clientPos++;
															}

														clientPos += clientStep;
													}
											}
										else if(col.type == VCL_HT_GENERIC_COLOR)
											{
												for(unsigned int yindex = 0; yindex < currHgh->value && yindex + currY < height; yindex++)
													{
														for(unsigned int xindex = 0; xindex < currWdh->value && xindex + currX < width; xindex++)
															{
																tmpCol = ((VCLGenericColor*)(col.handle))->getColor	(
																														clientPos % width,
																														clientPos / width,
																														width,
																														height
																													);

																OPT_SET_TRANSP_PIX	(
																						clientPos,
																						tmpCol.b,
																						tmpCol.g,
																						tmpCol.r,
																						tmpCol.a,
																						sprite
																					);

																//sprite[clientPos] = col;

																clientPos++;
															}

														clientPos += clientStep;
													}
											}
	
										//col

										/*if(currCell->marked == VCL_SELECTED)
											col = currCell->txtSelColor;*/
										if(currCell->marked == VCL_MARKED)
											col = capMarkColor;
										else
											col = fixCapColor;

										//cap

										if(parentWindow != VCLNULL && currCell->caption != VCLNULL)
											{
												if(currX + currWdh->value < width)
													clientWidth = currWdh->value;
												else
													clientWidth = width - currX;

												if(currY + currHgh->value < height)
													clientBottom = currHgh->value;
												else
													clientBottom = height - currY;

												carPos = currCell->caption->render(parentWindow, sprite, currX, currY, width, height, col, clientWidth, clientBottom, currCell->caretPos);
											}

										//caret

										if(currCell == selectedCell && caretOn == VCLACCEPT)
											{
												carPos.cx		+= currX;
												carPos.cy		+= currY;

												if(carPos.cx == currX + currWdh->value)
													carPos.cx -= 2;

												clientStep		= width - 2;

												clientWidth		= 2;
												clientBottom	= carPos.cy * width;

												clientPos		= currY * width + carPos.cx;

												while(clientPos < clientBottom)
													{
														for(unsigned int xPos = 0; xPos < clientWidth; xPos++)
															{
																sprite[clientPos].b = 255 - sprite[clientPos].b;
																sprite[clientPos].g = 255 - sprite[clientPos].g;
																sprite[clientPos].r = 255 - sprite[clientPos].r;

																					/*{
																						255 - sprite[clientPos].b,
																						255 - sprite[clientPos].g,
																						255 - sprite[clientPos].r,
																						sprite[clientPos].a
																					};*/
																clientPos++;
															}

														clientPos += clientStep;
													}
											}

										//caret

										currX		+=	currWdh->value + gridWidth;
										currCellNum	++;

										currCell	=	currCell->next;
										currWdh		=	currWdh->next;
									}
						
								while(currCell != VCLNULL && currCellNum < firstCellRender && currX < width)
									{
										//currX		+= currWdh->value + gridWidth;
										currCellNum	++;

										currCell	= currCell->next;
										currWdh		=	currWdh->next;
									}

								while(currCell != VCLNULL && currX < width)
									{
										//col

										if(currCell->marked == VCL_SELECTED)
											col = currCell->selColor;
										else if(currCell->marked == VCL_MARKED)
											col = bkMarkColor;
										else
											col = currCell->color;

										//rect

										clientPos = currY * width + currX;

										if(currX + currWdh->value > width)
											clientStep = currX;
										else
											clientStep = width - currWdh->value;

										if(col.type == VCL_HT_RGBA_COLOR)
											{
												for(unsigned int yindex = 0; yindex < currHgh->value && yindex + currY < height; yindex++)
													{
														for(unsigned int xindex = 0; xindex < currWdh->value && xindex + currX < width; xindex++)
															{
																OPT_SET_TRANSP_PIX	(
																						clientPos,
																						((RGBAPix*)(col.handle))->b,
																						((RGBAPix*)(col.handle))->g,
																						((RGBAPix*)(col.handle))->r,
																						((RGBAPix*)(col.handle))->a,
																						sprite
																					);

																//sprite[clientPos] = col;

																clientPos++;
															}

														clientPos += clientStep;
													}
											}
										else if(col.type == VCL_HT_GENERIC_COLOR)
											{
												for(unsigned int yindex = 0; yindex < currHgh->value && yindex + currY < height; yindex++)
													{
														for(unsigned int xindex = 0; xindex < currWdh->value && xindex + currX < width; xindex++)
															{
																tmpCol = ((VCLGenericColor*)(col.handle))->getColor	(
																														clientPos % width,
																														clientPos / width,
																														width,
																														height
																													);

																OPT_SET_TRANSP_PIX	(
																						clientPos,
																						tmpCol.b,
																						tmpCol.g,
																						tmpCol.r,
																						tmpCol.a,
																						sprite
																					);

																//sprite[clientPos] = col;

																clientPos++;
															}

														clientPos += clientStep;
													}
											}
	
										//col

										if(currCell->marked == VCL_SELECTED)
											col = currCell->txtSelColor;
										else if(currCell->marked == VCL_MARKED)
											col = capMarkColor;
										else
											col = currCell->txtColor;

										//cap

										if(parentWindow != VCLNULL && currCell->caption != VCLNULL)
											{
												if(currX + currWdh->value < width)
													clientWidth = currWdh->value;
												else
													clientWidth = width - currX;

												if(currY + currHgh->value < height)
													clientBottom = currHgh->value;
												else
													clientBottom = height - currY;

												carPos = currCell->caption->render(parentWindow, sprite, currX, currY, width, height, col, clientWidth, clientBottom, currCell->caretPos);
											}

										//caret

										if(currCell == selectedCell && caretOn == VCLACCEPT)
											{
												carPos.cx		+= currX;
												carPos.cy		+= currY;

												if(carPos.cx == currX + currWdh->value)
													carPos.cx -= 2;

												clientStep		= width - 2;

												clientWidth		= 2;
												clientBottom	= carPos.cy * width;

												clientPos		= currY * width + carPos.cx;

												while(clientPos < clientBottom)
													{
														for(unsigned int xPos = 0; xPos < clientWidth; xPos++)
															{
																sprite[clientPos].b = 255 - sprite[clientPos].b;
																sprite[clientPos].g = 255 - sprite[clientPos].g;
																sprite[clientPos].r = 255 - sprite[clientPos].r;

																					/*
																					{
																						255 - sprite[clientPos].b,
																						255 - sprite[clientPos].g,
																						255 - sprite[clientPos].r,
																						sprite[clientPos].a
																					};
																					*/
																clientPos++;
															}

														clientPos += clientStep;
													}
											}

										//caret

										currX		+= currWdh->value + gridWidth;
										currCell	= currCell->next;
										currWdh		=	currWdh->next;
									}

								currY += currHgh->value + gridWidth;
							}

						currRow = currRow->next;
						currHgh	= currHgh->next;
					}
			}
	}

void VCLTextGrid::addToEndRow(unsigned int inHeight)
	{
		VCL_TEXT_ROW*	tmpRow = new VCL_TEXT_ROW;
		VCL_TEXT_CELL*	tmpCell = VCLNULL;

		for(unsigned int index = 0; index < colCount; index++)
			{
				if(tmpCell == VCLNULL)
					{
						tmpCell = new VCL_TEXT_CELL;

						tmpCell->prev		= VCLNULL;
						
						tmpCell->caption	= new VCLString(L"");
						tmpCell->caretPos	= 0;
						tmpCell->color		= bkColor;
						tmpCell->selColor	= bkSelColor;
						tmpCell->txtColor	= capColor;
						tmpCell->txtSelColor= capSelColor;
						tmpCell->marked		= VCLNULL;

						tmpRow->cell		= tmpCell;
					}
				else
					{
						tmpCell->next		= new VCL_TEXT_CELL;
						tmpCell->next->prev	= tmpCell;

						tmpCell				= tmpCell->next;
						
						tmpCell->caption	= new VCLString(L"");
						tmpCell->caretPos	= 0;
						tmpCell->color		= bkColor;
						tmpCell->selColor	= bkSelColor;
						tmpCell->txtColor	= capColor;
						tmpCell->txtSelColor= capSelColor;
						tmpCell->marked		= VCLNULL;
					}
			}

		tmpCell->next		= VCLNULL;

		tmpRow->next		= VCLNULL;
		tmpRow->prev		= lastRow;
		
		if(lastRow != VCLNULL)
			lastRow->next	= tmpRow;
		else
			rows			= tmpRow;

		lastRow				= tmpRow;

		VCL_SIZE_VALUE* newHeight	= new VCL_SIZE_VALUE;

		newHeight->value			= inHeight;
		newHeight->next				= VCLNULL;

		if(lastHeight != VCLNULL)
			lastHeight->next		= newHeight;
		else
			heightList				= newHeight;
		
		lastHeight					= newHeight;

		rowCount++;
	}

void VCLTextGrid::deleteLastRow()
	{
		if(lastRow != VCLNULL)
			{
				VCL_TEXT_ROW* newHead			= lastRow->prev;

				if(lastRow == selectedRow)
					{
						selectedRow				= VCLNULL;
						selectedCell			= VCLNULL;
					}

				delete lastRow;

				lastRow							= newHead;

				if(lastRow != VCLNULL)
					lastRow->next				= VCLNULL;
				else
					rows						= VCLNULL;

				VCL_SIZE_VALUE* currVal			= heightList;
				VCL_SIZE_VALUE*	newHeightHead	= VCLNULL;

				if(currVal != VCLNULL)
					{
						while(currVal->next != VCLNULL)
							{
								newHeightHead	= currVal;
								currVal			= currVal->next;
							}
					}
				else
					newHeightHead				= VCLNULL;

				delete lastHeight;

				lastHeight						= newHeightHead;

				if(lastHeight != VCLNULL)
					lastHeight->next				= VCLNULL;
				else
					heightList = VCLNULL;

				rowCount--;
			}
	}

void VCLTextGrid::addToStartRow(unsigned int inHeight)
	{
		VCL_TEXT_ROW*	tmpRow = new VCL_TEXT_ROW;
		VCL_TEXT_CELL*	tmpCell = VCLNULL;

		for(unsigned int index = 0; index < colCount; index++)
			{
				if(tmpCell == VCLNULL)
					{
						tmpCell = new VCL_TEXT_CELL;

						tmpCell->prev		= VCLNULL;
						
						tmpCell->caption	= new VCLString(L"");
						tmpCell->caretPos	= 0;
						tmpCell->color		= bkColor;
						tmpCell->selColor	= bkSelColor;
						tmpCell->txtColor	= capColor;
						tmpCell->txtSelColor= capSelColor;
						tmpCell->marked		= VCLNULL;

						tmpRow->cell		= tmpCell;
					}
				else
					{
						tmpCell->next		= new VCL_TEXT_CELL;
						tmpCell->next->prev	= tmpCell;

						tmpCell				= tmpCell->next;
						
						tmpCell->caption	= new VCLString(L"");
						tmpCell->caretPos	= 0;
						tmpCell->color		= bkColor;
						tmpCell->selColor	= bkSelColor;
						tmpCell->txtColor	= capColor;
						tmpCell->txtSelColor= capSelColor;
						tmpCell->marked		= VCLNULL;
					}
			}

		tmpCell->next		= VCLNULL;

		tmpRow->next		= rows;
		tmpRow->prev		= VCLNULL;
		
		if(rows != VCLNULL)
			rows->prev		= tmpRow;
		else
			lastRow			= tmpRow;

		rows				= tmpRow;

		VCL_SIZE_VALUE* newHeight	= new VCL_SIZE_VALUE;

		newHeight->value			= inHeight;
		newHeight->next				= heightList;

		if(heightList == VCLNULL)
			lastHeight				= newHeight;

		heightList					= newHeight;

		rowCount++;
		selY++;

		//rebuildSprite(VCLNOALLOC);
	}

void VCLTextGrid::deleteFirstRow()
	{
		if(rows != VCLNULL)
			{
				VCL_TEXT_ROW* newHead			= rows->next;

				if(rows == selectedRow)
					{
						selectedRow				= VCLNULL;
						selectedCell			= VCLNULL;
					}

				delete rows;

				rows							= newHead;

				if(rows != VCLNULL)
					rows->prev					= VCLNULL;
				else
					lastRow						= VCLNULL;

				VCL_SIZE_VALUE*	newHeightHead	= heightList->next;

				delete heightList;

				heightList						= newHeightHead;

				if(heightList == VCLNULL)
					lastHeight = VCLNULL;

				rowCount--;
				selY--;

				if(firstRowRender >= rowCount && firstRowRender > 0)
					firstRowRender--;

				//rebuildSprite(VCLNOALLOC);
			}
	}

void VCLTextGrid::focus()
	{
		VCLComponent::focus();

		/*if(rows != VCLNULL && rows->cell != VCLNULL)
			{
				rows->cell->marked = VCL_MARKED;
				selectedCell = rows->cell;
			}*/
	}

void VCLTextGrid::unfocus()
	{
		VCLComponent::unfocus();

		if(selectedCell != VCLNULL)
			{
				selectedCell->marked = VCLNULL;
				selectedCell = VCLNULL;
			}
	}

unsigned int VCLTextGrid::processCaret(LPVOID param)
	{
		while(true)
			{
				Sleep(500);

				//WaitForSingleObject(caretRenderMutex, INFINITE);
				
				if(selectedCell != VCLNULL)
					{
						if(selectedCell->marked != VCL_SELECTED)
							{
								caretOn = VCLNULL;

								//ReleaseMutex(caretRenderMutex);

								break;
							}
					}
				else
					{
						caretOn = VCLNULL;		

						//ReleaseMutex(caretRenderMutex);

						break;
					}

				if(caretBlinkOn == VCLACCEPT)
					{
						if(caretOn == VCLACCEPT)
							caretOn = VCLNULL;
						else
							caretOn = VCLACCEPT;

						rebuildSprite(VCLNOALLOC);
						
						update();
					}

				//ReleaseMutex(caretRenderMutex);
			}

		isThreadExist = VCLNULL;

		return id; //DEBUG
	}

DWORD VCLTextGrid::newThreadMethod(VCL_TXTGRID_THREAD_METHOD_PARAM* mParam)
	{
		VCLTextGrid*				pObject = mParam->object;
		VCL_TXTGRID_THREAD_METHOD	pMethod = mParam->method;
		LPVOID						pParam	= mParam->param;

		DWORD						res		= (pObject->*pMethod)(pParam);

		delete mParam;
		return res;
	}

char VCLTextGrid::react(unsigned int msg, unsigned int wParam, unsigned int lParam)
	{
		char res = VCLNULL;

		switch(msg)
			{
				case VCL_LBTN_DOWN:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(xPos >= x && xPos < x + (int)width && yPos >= y && yPos < y + (int)height)
							{
								//***	pushed render
								
								if(renderList->getSelectedComp() != this)
									{
										if(renderList->getSelectedComp() != VCLNULL)
											renderList->getSelectedComp()->unfocus();

										focus();
									}

								/*
									find and mark cell
								*/

								xPos					-= x;
								yPos					-= y;

								unsigned int cell		= 0;

								unsigned int currVal;

								VCL_TEXT_ROW*	currRow = rows;
								VCL_TEXT_CELL*	currCell = VCLNULL;
								VCL_SIZE_VALUE*	val		= heightList;

								currVal					= gridWidth;

								while(currRow != VCLNULL && cell < fixRowCount)
									{
										currVal += val->value + gridWidth;
										val		= val->next;
										currRow	= currRow->next;
										cell++;
									}

								if(yPos > currVal)
									{
										while(currRow != VCLNULL && cell < firstRowRender)
											{
												val		= val->next;
												currRow	= currRow->next;
												cell++;
											}

										while(currRow != VCLNULL)
											{
												if(currVal > yPos)
													{
														currRow = VCLNULL;
														break;
													}

												if(currVal + val->value >= yPos)
													break;

												currVal += val->value + gridWidth;
												val		= val->next;
												currRow	= currRow->next;
												cell++;
											}

										if(currRow != VCLNULL)
											{
												selectedRow	= currRow;
												selY		= cell;
												currVal		= gridWidth;
												cell		= 0;
												currCell	= currRow->cell;
												val			= widthList;

												while(currCell != VCLNULL && cell < fixColCount)
													{
														currVal		+= val->value + gridWidth;
														val			= val->next;
														currCell	= currCell->next;
														cell++;
													}

												if(xPos > currVal)
													{
														while(currCell != VCLNULL && cell < firstCellRender)
															{
																val			= val->next;
																currCell	= currCell->next;
																cell++;
															}

														while(currCell != VCLNULL)
															{
																if(currVal > xPos)
																	{
																		currCell = VCLNULL;
																		break;
																	}

																if(currVal + val->value >= xPos)
																	break;

																currVal		+= val->value + gridWidth;
																val			= val->next;
																currCell	= currCell->next;
																cell++;
															}

														if(currCell != VCLNULL)
															{
																selX	= cell;

																if(rowSelect == VCLNULL)
																	{
																		if(currCell->marked == VCLNULL)
																			{
																				WaitForSingleObject(caretRenderMutex, INFINITE);

																				if(selectedCell != VCLNULL)
																					selectedCell->marked	= VCLNULL;

																				caretOn				= VCLNULL;
																				caretBlinkOn		= VCLNULL;

																				currCell->marked	= VCL_MARKED;
																				selectedCell		= currCell;

																				ReleaseMutex(caretRenderMutex);
																			}
																		else if(currCell->marked == VCL_MARKED && readOnly == VCLNULL)
																			{
																				WaitForSingleObject(caretRenderMutex, INFINITE);

																				currCell->marked	= VCL_SELECTED;

																				caretOn				= VCLACCEPT;
																				caretBlinkOn		= VCLACCEPT;

																				if(isThreadExist == VCLNULL)
																					{
																						VCL_TXTGRID_THREAD_METHOD_PARAM* mParam = new VCL_TXTGRID_THREAD_METHOD_PARAM;

																						mParam->method	= &VCLTextGrid::processCaret; 
																						mParam->object	= this;
																						mParam->param	= VCLNULL;

																						isThreadExist	= VCLACCEPT;

																						CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)newThreadMethod, mParam, 0, NULL);
																					}

																				ReleaseMutex(caretRenderMutex);
																			}
																	}
															}
													}
											}
									}

								if(currCell != VCLNULL)
									{
										rebuildSprite(VCLNOALLOC);
										update();
									}

								//***	

								if(MouseLDownHandle != VCLNULL)
									MouseLDownHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_LBTN_UP:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(renderList->getSelectedComp() == this)
							{
								if(MouseLUpHandle != VCLNULL)
									MouseLUpHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_RBTN_DOWN:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(xPos >= x && xPos < x + (int)width && yPos >= y && yPos < y + (int)height)
							{
								if(MouseRDownHandle != VCLNULL)
									MouseRDownHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_RBTN_UP:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(renderList->getSelectedComp() == this)
							{
								if(MouseRUpHandle != VCLNULL)
									MouseRUpHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_MBTN_DOWN:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(xPos >= x && xPos < x + (int)width && yPos >= y && yPos < y + (int)height)
							{
								if(MouseMDownHandle != VCLNULL)
									MouseMDownHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_MBTN_UP:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(renderList->getSelectedComp() == this)
							{	
								if(MouseMUpHandle != VCLNULL)
									MouseMUpHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_MMOVE:
					{
						short xPos = (short)(lParam); 
						short yPos = (short)(lParam >> 16);

						if(xPos >= x && xPos < x + (int)width && yPos >= y && yPos < y + (int)height)
							{
								if(MouseMoveHandle != VCLNULL)
									MouseMoveHandle(this, xPos, yPos);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_SCROLL:
					{
						if(((POINT*)lParam)->x >= x && ((POINT*)lParam)->x < x + (int)width && ((POINT*)lParam)->y >= y && ((POINT*)lParam)->y < y + (int)height)
							{
								if((short)(wParam >> 16) < 0)
									{
										if(selectedCell != VCLNULL && selectedCell->marked == VCL_MARKED && selectedRow->next != VCLNULL)
											{
												selectedCell->marked = VCLNULL;

												selectedRow = selectedRow->next;

												selectedCell = selectedRow->cell;

												for(unsigned int index = 0; index < selX; index++)
													selectedCell = selectedCell->next;

												selectedCell->marked = VCL_MARKED;

												selY++;

												VCL_SIZE_VALUE* currVal = heightList;

												unsigned int currY = 0;

												for(unsigned int index = 0; index < fixRowCount; index++)
													{
														currY += currVal->value + gridWidth;
														currVal = currVal->next;
													}

												for(unsigned int index = fixRowCount; index < firstRowRender; index++)
													currVal = currVal->next;

												for(unsigned int index = firstRowRender; index <= selY; index++)
													{
														currY += currVal->value + gridWidth;
														currVal = currVal->next;
													}

												if(currY >= height)
													firstRowRender++;

												rebuildSprite(VCLNOALLOC);
												update();
											}

										if(MouseScrollUpHandle != VCLNULL)
											MouseScrollUpHandle(this, (long)wParam, (long)lParam);
									}
								else
									{
										if(selectedCell != VCLNULL && selectedCell->marked == VCL_MARKED && selectedRow->prev != VCLNULL && selY > fixRowCount)
											{
												selectedCell->marked = VCLNULL;

												selectedRow = selectedRow->prev;

												selectedCell = selectedRow->cell;

												for(unsigned int index = 0; index < selX; index++)
													selectedCell = selectedCell->next;

												selectedCell->marked = VCL_MARKED;

												if(selY <= firstRowRender && firstRowRender > fixRowCount)
													firstRowRender--;

												selY--;

												rebuildSprite(VCLNOALLOC);
												update();
											}

										if(MouseScrollDownHandle != VCLNULL)
											MouseScrollDownHandle(this, (long)wParam, (long)lParam);
									}

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_KEY_DOWN:
					{
						if(renderList->getSelectedComp() == this)
							{
								switch(wParam)
									{
										case VK_DELETE:
											{
												if(selectedCell != VCLNULL && selectedCell->marked == VCL_SELECTED && selectedCell->caption != VCLNULL)
													{
														WaitForSingleObject(caretRenderMutex, INFINITE);

														selectedCell->caption->delChar(selectedCell->caretPos);
														rebuildSprite(VCLNOALLOC);
														update();

														ReleaseMutex(caretRenderMutex);
													}
											
												break;
											}
										case VK_DOWN:
											{
												if(selectedCell != VCLNULL && selectedCell->marked == VCL_MARKED && selectedRow->next != VCLNULL)
													{
														selectedCell->marked = VCLNULL;

														selectedRow = selectedRow->next;

														selectedCell = selectedRow->cell;

														for(unsigned int index = 0; index < selX; index++)
															selectedCell = selectedCell->next;

														selectedCell->marked = VCL_MARKED;

														selY++;

														VCL_SIZE_VALUE* currVal = heightList;

														unsigned int currY = 0;

														for(unsigned int index = 0; index < fixRowCount; index++)
															{
																currY += currVal->value + gridWidth;
																currVal = currVal->next;
															}

														for(unsigned int index = fixRowCount; index < firstRowRender; index++)
															currVal = currVal->next;

														for(unsigned int index = firstRowRender; index <= selY; index++)
															{
																currY += currVal->value + gridWidth;
																currVal = currVal->next;
															}

														if(currY >= height)
															firstRowRender++;

														rebuildSprite(VCLNOALLOC);
														update();
													}

												break;
											}
										case VK_UP:
											{
												if(selectedCell != VCLNULL && selectedCell->marked == VCL_MARKED && selectedRow->prev != VCLNULL && selY > fixRowCount)
													{
														selectedCell->marked = VCLNULL;

														selectedRow = selectedRow->prev;

														selectedCell = selectedRow->cell;

														for(unsigned int index = 0; index < selX; index++)
															selectedCell = selectedCell->next;

														selectedCell->marked = VCL_MARKED;

														if(selY <= firstRowRender && firstRowRender > fixRowCount)
															firstRowRender--;

														selY--;

														rebuildSprite(VCLNOALLOC);
														update();
													}

												break;
											}
										case VK_LEFT:
											{
												WaitForSingleObject(caretRenderMutex, INFINITE);

												if(isThreadExist)
													{
														if(selectedCell != VCLNULL && selectedCell->marked == VCL_SELECTED && selectedCell->caption != VCLNULL)
															{
																if(selectedCell->caretPos > 0)
																	selectedCell->caretPos--;

																if((unsigned short)lParam == 1)
																	{
																		caretOn			= VCLACCEPT;
																		caretBlinkOn	= VCLNULL;
																	}

																rebuildSprite(VCLNOALLOC);
																update();
															}
													}
												else if(selectedCell != VCLNULL && selectedCell->marked == VCL_MARKED && selectedCell->prev != VCLNULL && selX > fixColCount)
													{
														selectedCell->marked = VCLNULL;

														selectedCell = selectedCell->prev;

														selectedCell->marked = VCL_MARKED;

														if(selX <= firstCellRender && firstCellRender > fixColCount)
															firstCellRender--;

														selX--;

														rebuildSprite(VCLNOALLOC);
														update();
													}

												ReleaseMutex(caretRenderMutex);

												break;
											}
										case VK_RIGHT:
											{
												WaitForSingleObject(caretRenderMutex, INFINITE);

												if(isThreadExist)
													{
														if(selectedCell != VCLNULL && selectedCell->marked == VCL_SELECTED && selectedCell->caption != VCLNULL)
															{
																if(selectedCell->caretPos < selectedCell->caption->getLength())
																	selectedCell->caretPos++;

																if((unsigned short)lParam == 1)
																	{
																		WaitForSingleObject(caretRenderMutex, INFINITE);
													
																		caretOn			= VCLACCEPT;
																		caretBlinkOn	= VCLNULL;

																		ReleaseMutex(caretRenderMutex);
																	}

																rebuildSprite(VCLNOALLOC);
																update();
															}
													}
												else if(selectedCell != VCLNULL && selectedCell->marked == VCL_MARKED && selectedCell->next != VCLNULL)
													{
														selectedCell->marked = VCLNULL;

														selectedCell = selectedCell->next;

														selectedCell->marked = VCL_MARKED;

														selX++;

														VCL_SIZE_VALUE* currVal = widthList;

														unsigned int currX = 0;

														for(unsigned int index = 0; index < fixColCount; index++)
															{
																currX += currVal->value + gridWidth;
																currVal = currVal->next;
															}

														for(unsigned int index = fixColCount; index < firstCellRender; index++)
															currVal = currVal->next;

														for(unsigned int index = firstCellRender; index <= selX; index++)
															{
																currX += currVal->value + gridWidth;
																currVal = currVal->next;
															}

														if(currX >= width)
															firstCellRender++;

														rebuildSprite(VCLNOALLOC);
														update();
													}

												ReleaseMutex(caretRenderMutex);

												break;
											}
										case VK_TAB:
											{
												WaitForSingleObject(caretRenderMutex, INFINITE);

												unfocus();

												if(next != VCLNULL)
													next->focus();
												else
													(renderList->getFirstInList())->focus();

												update();	

												ReleaseMutex(caretRenderMutex);
											}
									}

								if(KeyDownHandle != VCLNULL)
									KeyDownHandle(this, wParam);
 
								res = VCLACCEPT;
							}

						break;
					}
				case VCL_CHAR:
					{
						if(renderList->getSelectedComp() == this)
							{
								switch (wParam)
									{
										case 8:	//backspace 
											{
												WaitForSingleObject(caretRenderMutex, INFINITE);

												if(selectedCell != VCLNULL && selectedCell->marked == VCL_SELECTED)
													{
														if((unsigned short)lParam == 1)
															{
																caretOn			= VCLACCEPT;
																caretBlinkOn	= VCLNULL;
															}

														if(selectedCell->caretPos > 0)
															{
																selectedCell->caption->delChar(selectedCell->caretPos - 1);
																selectedCell->caretPos--;
															}

														rebuildSprite(VCLNOALLOC);
													}

												ReleaseMutex(caretRenderMutex);

												break;
											}
										case 27: //esc
											{
												WaitForSingleObject(caretRenderMutex, INFINITE);

												if(selectedCell != VCLNULL && selectedCell->marked == VCL_SELECTED)
													{
														caretOn						= VCLNULL;
														selectedCell->caretPos		= 0;

														selectedCell->marked		= VCL_MARKED;
														//selectedCell				= VCLNULL;

														rebuildSprite(VCLNOALLOC);
													}

												ReleaseMutex(caretRenderMutex);

												break;
											}
										case 13: //enter
											{
												WaitForSingleObject(caretRenderMutex, INFINITE);

												if(selectedCell != VCLNULL)
													{
														if(selectedCell->marked == VCL_SELECTED)
															{
																caretOn						= VCLNULL;
																selectedCell->caretPos		= 0;

																selectedCell->marked		= VCL_MARKED;

																rebuildSprite(VCLNOALLOC);
															}
														else if(isThreadExist == VCLNULL && readOnly == VCLNULL)
															{
																caretOn			= VCLACCEPT;
																caretBlinkOn	= VCLACCEPT;

																selectedCell->marked = VCL_SELECTED;

																VCL_TXTGRID_THREAD_METHOD_PARAM* mParam = new VCL_TXTGRID_THREAD_METHOD_PARAM;

																mParam->method	= &VCLTextGrid::processCaret; 
																mParam->object	= this;
																mParam->param	= VCLNULL;

																isThreadExist	= VCLACCEPT;

																CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)newThreadMethod, mParam, 0, NULL);

																rebuildSprite(VCLNOALLOC);
															}
													}

												ReleaseMutex(caretRenderMutex);

												break;
											}
										default:
											{
												WaitForSingleObject(caretRenderMutex, INFINITE);

												if(selectedCell != VCLNULL && selectedCell->marked == VCL_SELECTED)
													{
														if((unsigned short)lParam == 1)
															{
																//WaitForSingleObject(caretRenderMutex, INFINITE);
														
																caretOn			= VCLACCEPT;
																caretBlinkOn	= VCLNULL;

																//ReleaseMutex(caretRenderMutex);
															}

														selectedCell->caption->insChar(selectedCell->caretPos, wParam);
														selectedCell->caretPos++;
													}

												rebuildSprite(VCLNOALLOC);

												ReleaseMutex(caretRenderMutex);
											}
									}

								update();

								if(CharTypedHandle != VCLNULL)
									CharTypedHandle(this, wParam);

								res = VCLACCEPT;
							}

						break;
					}
				case VCL_KEY_UP:
					{
						if(renderList->getSelectedComp() == this)
							{
								WaitForSingleObject(caretRenderMutex, INFINITE);

								if(selectedCell != VCLNULL && selectedCell->marked == VCL_SELECTED)
									caretBlinkOn	= VCLACCEPT;

								ReleaseMutex(caretRenderMutex);

								if(KeyUpHandle != VCLNULL)
									KeyUpHandle(this, wParam);

								res = VCLACCEPT;
							}
					}
			}

		return res;
	}