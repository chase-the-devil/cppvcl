
/*
\***	Made by Cas.
*/

//***
//	needed:
//		VCLButton
//		VCLTextField
//		VCLSubWindow
//		VCLColorDiagram
//			3 conditions:
//				r-b, r-g, b-g
//			switcher:
//				space
/*
			color field => reaction on mouse click: set coordinates; => reaction on arrows: chnage coordinates.

			3 text fields: r, g, b, a

			color rect: displayed current color + a; background grid.
*/
//***

#ifndef			__CPP_VCL__

	#define		__CPP_VCL__

	#include	<Windows.h>
	#include	<math.h>

	//**		basic true/false types

	#define		VCLNULL							0
	#define		VCLACCEPT						~VCLNULL

	//**		sprite creation mods

	#define		VCLALLOC						0
	#define		VCLREALLOC						1
	#define		VCLNOALLOC						2

	//**		window conditions types

	#define		VCLOPENING						0
	#define		VCLOPENED						1
	#define		VCLCLOSING						2
	#define		VCLCLOSED						3

	//**		message types

	#define		VCL_LBTN_DOWN					0
	#define		VCL_RBTN_DOWN					1
	#define		VCL_MBTN_DOWN					2
	#define		VCL_LBTN_UP						3
	#define		VCL_RBTN_UP						4
	#define		VCL_MBTN_UP						5
	#define		VCL_MMOVE						6
	#define		VCL_SCROLL						7
	#define		VCL_KEY_DOWN					8
	#define		VCL_KEY_UP						9
	#define		VCL_CHAR						10

	//**		info types in expression

	#define		VCL_FNUM						0

	#define		VCL_LAST_NUM					0

	//**		operations types in expression

	#define		VCL_OP_NOT						VCL_LAST_NUM + 1
	#define		VCL_OP_PLUS						VCL_LAST_NUM + 2
	#define		VCL_OP_MINUS					VCL_LAST_NUM + 3
	#define		VCL_OP_DIV						VCL_LAST_NUM + 4
	#define		VCL_OP_MUL						VCL_LAST_NUM + 5
	#define		VCL_OP_MOD						VCL_LAST_NUM + 6
	#define		VCL_OP_SIN						VCL_LAST_NUM + 7
	#define		VCL_OP_TG						VCL_LAST_NUM + 8
	#define		VCL_OP_SQRT						VCL_LAST_NUM + 9
	#define		VCL_OP_POW						VCL_LAST_NUM + 10
	#define		VCL_OP_INT						VCL_LAST_NUM + 11
	#define		VCL_OP_BIT_NO					VCL_LAST_NUM + 12
	#define		VCL_OP_BIT_R_OFF				VCL_LAST_NUM + 13
	#define		VCL_OP_BIT_L_OFF				VCL_LAST_NUM + 14
	#define		VCL_OP_BIT_AND					VCL_LAST_NUM + 15
	#define		VCL_OP_BIT_OR					VCL_LAST_NUM + 16
	#define		VCL_OP_BIT_XOR					VCL_LAST_NUM + 17

	#define		VCL_OP_LAST						VCL_LAST_NUM + 17

	//**		var types

	#define		VCL_VAR_X						VCL_OP_LAST + 1
	#define		VCL_VAR_Y						VCL_OP_LAST + 2
	#define		VCL_VAR_WIDTH					VCL_OP_LAST + 3
	#define		VCL_VAR_HEIGHT					VCL_OP_LAST + 4

	#define		VCL_NO_OP						VCL_OP_LAST + 5

	//**		handle types

	//			1 - byte

	#define		VCL_HT_INT8						0
	#define		VCL_HT_BOOL						1
	#define		VCL_HT_CHAR						2
	#define		VCL_HT_U_CHAR					3
	#define		VCL_HT_S_CHAR					4

	//			2 - bytes

	#define		VCL_HT_INT16					5
	#define		VCL_HT_SHORT					6
	#define		VCL_HT_U_SHORT					7
	#define		VCL_HT_WCHAR_T					8

	//			4 - bytes

	#define		VCL_HT_INT32					9
	#define		VCL_HT_INT						10
	#define		VCL_HT_U_INT					11
	#define		VCL_HT_LONG						12
	#define		VCL_HT_U_LONG					13
	#define		VCL_HT_FLOAT					14

	//			8 - bytes

	#define		VCL_HT_INT64					15
	#define		VCL_HT_LONG_LONG				16
	#define		VCL_HT_DOUBLE					17
	#define		VCL_HT_LONG_DOUBLE				18

	//			16 - bytes

	#define		VCL_HT_INT128					19

	//			VCL - types

	#define		VCL_HT_RGBA_COLOR				20
	#define		VCL_HT_GENERIC_COLOR			21

	//**		selection types

	#define		VCL_MARKED						VCLNULL + 1
	#define		VCL_SELECTED					VCLNULL	+ 2

	//***		macro

	//***		basic standart of 4-byte pixel color setting
	//***
	//PIX_B: *((unsigned char*)&(pixels[y * width + x]))
	//PIX_G: *((unsigned char*)((int)&(pixels[y * width + x]) + sizeof(unsigned char)))
	//PIX_R: *((unsigned char*)((int)&(pixels[y * width + x]) + sizeof(unsigned char) * 2))
	//PIX_A: *((unsigned char*)((int)&(pixels[y * width + x]) + sizeof(unsigned char) * 3))
	//***
	
	#define		GET_PIX_COLOR(b, g, r) \
					((unsigned long)((unsigned long)((unsigned long)(b << 8) | g) << 8) | r)

	#define		GET_PIX_TRANSP_COLOR(b, g, r, a) \
					((unsigned long)((unsigned long)((unsigned long)((unsigned long)((unsigned long)(a << 8) | b) << 8) | g) << 8) | r)

	//***		here used macro for RGBAPix struct standart

	#define		GET_PIX_B(x, y, pixels, width)	\
					pixels[y * width + x].b

	#define		GET_PIX_G(x, y, pixels, width)	\
					pixels[y * width + x].g

	#define		GET_PIX_R(x, y, pixels, width)	\
					pixels[y * width + x].r

	#define		GET_PIX_A(x, y, pixels, width)	\
					pixels[y * width + x].a

	#define		SET_PIX(x, y, b, g, r, pixels, width)	\
					GET_PIX_B(x, y, pixels, width) = b;		\
					GET_PIX_G(x, y, pixels, width) = g;		\
					GET_PIX_R(x, y, pixels, width) = r

	#define		INC_PIX(x, y, b, g, r, pixels, width)	\
					GET_PIX_B(x, y, pixels, width) += b;	\
					GET_PIX_G(x, y, pixels, width) += g;	\
					GET_PIX_R(x, y, pixels, width) += r

	#define		DEC_PIX(x, y, b, g, r, pixels, width)	\
					GET_PIX_B(x, y, pixels, width) -= b;	\
					GET_PIX_G(x, y, pixels, width) -= g;	\
					GET_PIX_R(x, y, pixels, width) -= r

	#define		SET_TRANSP_PIX(x, y, b, g, r, a, pixels, width)	\
					DEC_PIX	(	\
								x,	\
								y,	\
								(((GET_PIX_B(x, y, pixels, width) - b) * a) / 255),	\
								(((GET_PIX_G(x, y, pixels, width) - g) * a) / 255),	\
								(((GET_PIX_R(x, y, pixels, width) - r) * a) / 255),	\
								pixels,	\
								width	\
							)

	//**		macro for optimizing work with pixels

	#define		OPT_GET_PIX_B(pos, pixels)	\
					pixels[pos].b

	#define		OPT_GET_PIX_G(pos, pixels)	\
					pixels[pos].g

	#define		OPT_GET_PIX_R(pos, pixels)	\
					pixels[pos].r

	#define		OPT_GET_PIX_A(pos, pixels)	\
					pixels[pos].a

	#define		OPT_SET_PIX(pos, b, g, r, pixels)	\
					OPT_GET_PIX_B(pos, pixels) = b;		\
					OPT_GET_PIX_G(pos, pixels) = g;		\
					OPT_GET_PIX_R(pos, pixels) = r

	#define		OPT_INC_PIX(pos, b, g, r, pixels)	\
					OPT_GET_PIX_B(pos, pixels) += b;	\
					OPT_GET_PIX_G(pos, pixels) += g;	\
					OPT_GET_PIX_R(pos, pixels) += r

	#define		OPT_DEC_PIX(pos, b, g, r, pixels)	\
					OPT_GET_PIX_B(pos, pixels) -= b;	\
					OPT_GET_PIX_G(pos, pixels) -= g;	\
					OPT_GET_PIX_R(pos, pixels) -= r

	#define		OPT_SET_TRANSP_PIX(pos, b, g, r, a, pixels)	\
					OPT_DEC_PIX		(	\
										pos,	\
										(((OPT_GET_PIX_B(pos, pixels) - b) * a) / 255),	\
										(((OPT_GET_PIX_G(pos, pixels) - g) * a) / 255),	\
										(((OPT_GET_PIX_R(pos, pixels) - r) * a) / 255),	\
										pixels	\
									)

	//***	class prototypes

	class VCLString;				//used for contain information about string data, and render it
	class VCLGenericColor;			//used for generate dynamic color

	class VCLComponent;				//base component class
	class VCLRenderQueue;			//used for rendering control and messages processing

	class VCLWindow;				//base window class
	
	class VCLButton;				//a button with custom parameters etc.

	class VCLTextField;				//

	class VCLTextGrid;

	//***	types

	//**	This types of call used for
	//**	run some methods of classes
	//**	in separate threads

	typedef 
		unsigned int (VCLWindow::*VCL_WINDOW_THREAD_METHOD		)	(LPVOID lParam);

	typedef 
		unsigned int (VCLTextField::*VCL_TXTFIELD_THREAD_METHOD	)	(LPVOID lParam);

	typedef 
		unsigned int (VCLTextGrid::*VCL_TXTGRID_THREAD_METHOD	)	(LPVOID lParam);

	//***	structures

	//**	WinAPI standart of 4-byte pixel

	struct RGBAPix
		{
			unsigned char b;
			unsigned char g;
			unsigned char r;
			unsigned char a;
		};

	struct VCL_BYTE_PAIR
		{
			char _1;
			char _2;
		};

	struct VCL_TYPED_HANDLE
		{
			char	type;
			void*	handle;
		};

	//**	This types used to pass parameters
	//**	in new threaded function

	struct VCL_WINDOW_THREAD_METHOD_PARAM
		{
			VCL_WINDOW_THREAD_METHOD		method;
			VCLWindow*						object;
			LPVOID							param;
		};

	struct VCL_TXTFIELD_THREAD_METHOD_PARAM
		{
			VCL_TXTFIELD_THREAD_METHOD		method;
			VCLTextField*					object;
			LPVOID							param;
		};

	struct VCL_TXTGRID_THREAD_METHOD_PARAM
		{
			VCL_TXTGRID_THREAD_METHOD		method;
			VCLTextGrid*					object;
			LPVOID							param;
		};

	//**	This type created for users to initialize a window

	struct VCL_WINDOW_CREATE_PARAMS
		{
			HINSTANCE			hInst;
			int					nCommandShow;
			LPCWSTR				wndClassName;
			LPCWSTR				wndCaption;
			int					wndStyle;
			unsigned int		wndWidth;
			unsigned int		wndHeight;
			int					wndX;
			int					wndY;
			VCL_TYPED_HANDLE	wndBkColor;
		};

	//**	This type created for users to initialize a button

	struct VCL_BUTTON_CREATE_PARAMS
		{
			HWND				parentWindow;
			VCLComponent*		parentComponent;
			VCLRenderQueue*		renderList;

			unsigned int		btnWidth;
			unsigned int		btnHeight;
			int					btnX;
			int					btnY;
			char				btnVisible;

			VCLString*			btnCaption;
			int					capX;
			int					capY;			

			unsigned int		btnBorderWidth;

			VCL_TYPED_HANDLE	btnBkColor;
			VCL_TYPED_HANDLE	btnCapColor;
			VCL_TYPED_HANDLE	btnBrdColor;
			VCL_TYPED_HANDLE	btnBkSelColor;
			VCL_TYPED_HANDLE	btnCapSelColor;
			VCL_TYPED_HANDLE	btnBrdSelColor;
		};

	struct VCL_TXTFIELD_CREATE_PARAMS
		{
			HWND			parentWindow;
			VCLComponent*	parentComponent;
			VCLRenderQueue*	renderList;

			unsigned int	fldWidth;
			unsigned int	fldHeight;
			int				fldX;
			int				fldY;
			char			fldVisible;

			VCLString*		fldCaption;

			unsigned int	fldBorderWidth;
			
			VCL_TYPED_HANDLE			fldBkColor;
			VCL_TYPED_HANDLE			fldCapColor;
			VCL_TYPED_HANDLE			fldBrdColor;
			VCL_TYPED_HANDLE			fldBkSelColor;
			VCL_TYPED_HANDLE			fldCapSelColor;
			VCL_TYPED_HANDLE			fldBrdSelColor;
		};

	struct VCL_DIALOG_CREATE_PARAMS
		{
			HWND			wndOwner;
			LPTSTR			filePath;
			DWORD			maxFilePath;
			LPCTSTR			defaultExt;
			LPCTSTR			filters;
			DWORD			filterIndex;
			LPCTSTR			initialDir;
			DWORD			options;
			DWORD			optionsEx;
			LPCTSTR			title;
		};

	struct VCL_TEXT_CELL
		{
			VCLString*		caption;

			unsigned int	caretPos;
			char			marked;

			VCL_TYPED_HANDLE			color;
			VCL_TYPED_HANDLE			selColor;
			VCL_TYPED_HANDLE			txtColor;
			VCL_TYPED_HANDLE			txtSelColor;

			VCL_TEXT_CELL*	next;
			VCL_TEXT_CELL*	prev;
		};

	struct VCL_TEXT_ROW
		{
			VCL_TEXT_CELL*	cell;
			VCL_TEXT_CELL*	lastCell;
			
			VCL_TEXT_ROW*	next;
			VCL_TEXT_ROW*	prev;
		};

	struct VCL_SIZE_VALUE
		{
			unsigned int	value;
			VCL_SIZE_VALUE* next;
		};

	struct VCL_TXTGRID_CREATE_PARAMS
		{
			HWND			parentWindow;
			VCLComponent*	parentComponent;
			VCLRenderQueue*	renderList;

			unsigned int	grdWidth;
			unsigned int	grdHeight;
			unsigned int	grdInitialCellWidth;
			unsigned int	grdInitialCellHeight;
			int				grdX;
			int				grdY;
			unsigned int	grdColCount;
			unsigned int	grdRowCount;
			unsigned int	grdFixColCount;
			unsigned int	grdFixRowCount;
			char			grdVisible;

			unsigned int	grdGridWidth;
			
			VCL_TYPED_HANDLE			grdBkColor;
			VCL_TYPED_HANDLE			grdCapColor;
			VCL_TYPED_HANDLE			grdGrdColor;
			VCL_TYPED_HANDLE			grdBkSelColor;
			VCL_TYPED_HANDLE			grdCapSelColor;
			VCL_TYPED_HANDLE			grdBkMarkColor;
			VCL_TYPED_HANDLE			grdCapMarkColor;
			VCL_TYPED_HANDLE			grdFixBkColor;
			VCL_TYPED_HANDLE			grdFixCapColor;
		};

	struct VCL_EXPRESSION_ELEM;

	struct VCL_BRANCH
		{
			VCL_EXPRESSION_ELEM*	elem;
			VCL_BRANCH*				next;
		};

	struct VCL_EXPRESSION_ELEM
		{
			char					type;
			void*					info;
			VCL_BRANCH*				branches;
		};

	//***	types

	typedef		RGBAPix*	Sprite;

	//***	classes

	//**	basic classes

	class VCLGenericColor
		{
			public:

										VCLGenericColor		();
										~VCLGenericColor	();

				RGBAPix					getColor			(unsigned int x, unsigned int y, unsigned int width, unsigned int height							);
				void					initGenericColor	(const char* bExpr, const char* gExpr, const char* rExpr, const char* aExpr							);

			protected:

				VCL_EXPRESSION_ELEM*	b;
				VCL_EXPRESSION_ELEM*	g;
				VCL_EXPRESSION_ELEM*	r;
				VCL_EXPRESSION_ELEM*	a;

				VCL_EXPRESSION_ELEM*	parseExpression		(const char* expr, unsigned int exprLen, unsigned int* reserved										);
				float					calculateExpression	(VCL_EXPRESSION_ELEM* head, unsigned int x, unsigned int y, unsigned int width, unsigned int height	);
				void					releaseExpression	(VCL_EXPRESSION_ELEM* head																			);
				
				VCL_BYTE_PAIR			isOperator			(const char* obj																					);
				bool					isNumeric			(const char* obj																					);
		};

	class VCLString
		{
			protected:

				//***		fields

				LPWSTR			string;
				unsigned int	length;

				HFONT			font;

			public:

				//***		methods

				//**		constructor & destructor

								VCLString	(LPCWSTR text																									);
								VCLString	(LPCWSTR text,  PLOGFONT fntParams																				);

								~VCLString	(																												);	

				unsigned int	getLength	(																												);
				LPCWSTR			getText		(																												);
				void			setText		(LPCWSTR		newText																							);
				void			setFont		(PLOGFONT		fntParams																						);

				void			addChar		(wchar_t		inChar																							);
				void			delChar		(unsigned int	inIndex																							);
				void			insChar		(unsigned int	inIndex, wchar_t inChar																			);
				void			concat		(LPCWSTR		inStr																							);
				

				SIZE			render		(
												HWND prntWindow,
												Sprite buffer,
												int inX,
												int inY,
												unsigned int bufWidth,
												unsigned int bufHeight,
												VCL_TYPED_HANDLE inCol,
												unsigned int clientWidth,
												unsigned int clientHeight,
												int customLen
											);
		};

	class VCLOpenDialog
		{
			public:
		
				HWND			hwndOwner;
				LPTSTR			filePath;
				DWORD			maxFilePath;
				LPCWSTR			initialDir;
				LPCWSTR			title;
				LPCWSTR			filters;
				LPCTSTR			defaultExt;
				DWORD			optionsEx;
				DWORD			options;
				DWORD			filterIndex;

								VCLOpenDialog	(VCL_DIALOG_CREATE_PARAMS*	inParams	);

				virtual bool	execute			(										);
		};

	class VCLSaveDialog : public VCLOpenDialog
		{
			public:
				
								VCLSaveDialog	(VCL_DIALOG_CREATE_PARAMS* inParams		);

				virtual bool	execute			(										) override;
		};

	//**		components

	class VCLComponent	abstract
		{
			public:

				//***	fields

				HWND			parentWindow;
				VCLComponent*	parentComponent;

				VCLComponent*	next;
				VCLComponent*	prev;

				void	(*MouseLDownHandle		)	(VCLComponent*	me, short mx, short mY										);
				void	(*MouseRDownHandle		)	(VCLComponent*	me, short mx, short mY										); 
				void	(*MouseMDownHandle		)	(VCLComponent*	me, short mx, short mY										); 
				void	(*MouseLUpHandle		)	(VCLComponent*	me, short mx, short mY										);
				void	(*MouseRUpHandle		)	(VCLComponent*	me, short mx, short mY										);
				void	(*MouseMUpHandle		)	(VCLComponent*	me, short mx, short mY										);
				void	(*MouseMoveHandle		)	(VCLComponent*	me, short mx, short mY										);
				void	(*MouseScrollDownHandle	)	(VCLComponent*	me, short mx, short mY										);
				void	(*MouseScrollUpHandle	)	(VCLComponent*	me, short mx, short mY										);
				void	(*KeyDownHandle			)	(VCLComponent*	me, unsigned int	vCode									);
				void	(*KeyUpHandle			)	(VCLComponent*	me, unsigned int	vCode									);
				void	(*SelectHandle			)	(VCLComponent*	me															);
				void	(*UnselectHandle		)	(VCLComponent*	me															);

				//***	methods

				//**	constructor & destructor

								VCLComponent
													(
														HWND			inParentWindow,
														VCLComponent*	inComponent,
														VCLRenderQueue*	inRenderList,
														unsigned int	inX,
														unsigned int	inY,
														unsigned int	inWidth,
														unsigned int	inHeight,
														char			inVisible
													);
								~VCLComponent		(																			);

				//**

				virtual void	render				(																			);
				
				//**		call this method after you set some visual parameter of component
				//**		but dont call it after each change, make all chnages then call it

				virtual void	refresh				(																			);
				virtual void	update				(																			);				

				virtual char	react				(unsigned int msg, unsigned int wParam, unsigned int lParam					)	abstract;

				int				getX				(																			);
				int				getY				(																			);
				unsigned int	getWidth			(																			);
				unsigned int	getHeight			(																			);
				unsigned int	getId				(																			);
				char			isVisible			(																			);
				char			isSelected			(																			);

				void			setPos				(int			newX,		int				newY							);
				void			setSize				(unsigned int	newWdh,		unsigned int	newHgh							);
				void			hide				(																			);
				void			show				(																			);

				virtual	void	focus				(																			);
				virtual	void	unfocus				(																			);

			protected:

				//***			fields
	
				HDC				localDC;
				HBITMAP			localBmp;

				VCLRenderQueue*	renderList;

				unsigned int	id;

				Sprite			sprite;
				
				int				x;
				int				y;
				unsigned int	width;
				unsigned int	height;

				char			visible;

				//***			methods

				virtual void	rebuildSprite		(char realloc																);
		};

	class VCLRenderQueue
		{
			public:

				//***		fields

				Sprite			hiddenBuffer;
				HDC				memdc;

				RECT			clientRect;

				//***		methods

				//**		constructor & destructor

								VCLRenderQueue	(															);
								~VCLRenderQueue	(															);

				//**

				void			addToStart		(VCLComponent*	newComponent								);
				void			addToEnd		(VCLComponent*	newComponent								);
				void			remove			(VCLComponent*	oldComponent								);
				void			freeList		(															);
				void			renderList		(															);

				unsigned int	getNewId		(															);

				void			selectComp		(VCLComponent*	inComponent									);
				VCLComponent*	getSelectedComp	(															);				

				char			sendMessage		(unsigned int msg, unsigned int wParam, unsigned int lParam	);

				VCLComponent*	getLastInList	(															);
				VCLComponent*	getFirstInList	(															);

			protected:

				//***		fields

				VCLComponent*	selectedComp;

				VCLComponent*	start;
				VCLComponent*	end;

				unsigned int	generator;
		};

	class VCLWindow
		{
			public:

				//***		fields

				//**		WinAPI declarations

				unsigned int					width;
				unsigned int					height;

				int								x;
				int								y;

				HBITMAP							hMemBmp;
				HDC								hdc;

				VCL_TYPED_HANDLE				bkColor;
				HWND							hWindow;

				VCLRenderQueue*					compList;

				char							closed;

				void	(*WindowRenderHandle		)	(VCLWindow*	me														);

				//**		constructor & destructor

												VCLWindow		(VCL_WINDOW_CREATE_PARAMS* wndParams					);
												~VCLWindow		(														);

				void							windowLiveCycle	();				

			protected:

				//***		fields

				HANDLE							windowMutex;
				HANDLE							renderMutex;

				//***		methods

				static LRESULT	__stdcall		WindowProc		(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM	lParam	);
				static DWORD					newThreadMethod	(VCL_WINDOW_THREAD_METHOD_PARAM*				mParam	);
				unsigned int					processMessages	(LPVOID											lParam	);

		};

	class VCLSubWindow : public VCLComponent
		{
			public:
				
				VCLRenderQueue*		compList;

				virtual char		react				(unsigned int msg, unsigned int wParam, unsigned int lParam	) override;

			protected:

				unsigned int		captionHeight;
				unsigned int		borderWidth;


				virtual void		rebuildSprite		(char realloc												) override;
		};

	//***
	//	1) Caption centering
	//***

	class VCLButton : public VCLComponent
		{
			public:
				
				//***		fields

				VCLString*					caption		= VCLNULL;

				VCL_TYPED_HANDLE			bkColor;
				VCL_TYPED_HANDLE			capColor;
				VCL_TYPED_HANDLE			brdColor;
				VCL_TYPED_HANDLE			bkSelColor;
				VCL_TYPED_HANDLE			capSelColor;
				VCL_TYPED_HANDLE			brdSelColor;

				//***		methods

				//**		constructor & destructor

								VCLButton			(VCL_BUTTON_CREATE_PARAMS* inParams							);
								~VCLButton			(															);
				//**

				virtual char	react				(unsigned int msg, unsigned int wParam, unsigned int lParam	) override;

				void			setBorderWidth		(unsigned int newBrdWidth									);
				unsigned int	getBorderWidth		(															);

				void			setCaptionX			(int inCapX													);
				void			setCaptionY			(int inCapY													);

				int				getCaptionX			(															);
				int				getCaptionY			(															);

			protected:

				//***		fields

				int				capX;
				int				capY;

				unsigned int	borderWidth;

				char			pressed;

				//***		methods

				virtual void	rebuildSprite		(char realloc												) override;
		};

	//***
	//	1)	SHIFT + LEFT, SHIFT + RIGHT : text selection.
	//	2)	Mouse click -> caret set up
	//	3)	Clip board working
	//***

	class VCLTextField : public VCLComponent
		{
			public:

				VCLString*		caption		= VCLNULL;

				VCL_TYPED_HANDLE			bkColor;
				VCL_TYPED_HANDLE			capColor;
				VCL_TYPED_HANDLE			brdColor;
				VCL_TYPED_HANDLE			bkSelColor;
				VCL_TYPED_HANDLE			capSelColor;
				VCL_TYPED_HANDLE			brdSelColor;

				void	(*CharTypedHandle						)	(VCLComponent*	me, wchar_t charValue						);

				//***

												VCLTextField  	(VCL_TXTFIELD_CREATE_PARAMS*	inParams					);
												~VCLTextField 	(															);

				virtual char					react		  	(unsigned int msg, unsigned int wParam, unsigned int lParam	) override;

				void							setBorderWidth	(unsigned int newBrdWidth									);
				unsigned int					getBorderWidth	(															);

				virtual void					focus			(															) override;
				virtual void					unfocus			(															) override;

			protected:

				char			caretOn;
				char			caretBlinkOn;
				char			isThreadExist;

				unsigned int	caretPos = 0;
				

				unsigned int	borderWidth;

				HANDLE			caretRenderMutex	= CreateMutex(NULL, false, NULL);

				//***

				virtual void					rebuildSprite	(char realloc												) override;

				unsigned int					processCaret	(LPVOID	param												);
				static DWORD					newThreadMethod	(VCL_TXTFIELD_THREAD_METHOD_PARAM*				mParam		);
		};

	//***		NOT FINISHED

	class VCLTextGrid : public VCLComponent
		{
			public:	

				VCL_TEXT_ROW*	rows;
				VCL_TEXT_ROW*	lastRow;

				VCL_TYPED_HANDLE			grdColor;
				VCL_TYPED_HANDLE			bkColor;
				VCL_TYPED_HANDLE			capColor;
				VCL_TYPED_HANDLE			bkSelColor;
				VCL_TYPED_HANDLE			capSelColor;
				VCL_TYPED_HANDLE			bkMarkColor;
				VCL_TYPED_HANDLE			capMarkColor;
				VCL_TYPED_HANDLE			fixBkColor;
				VCL_TYPED_HANDLE			fixCapColor;

				char			rowSelect;
				char			readOnly;
				unsigned int	gridWidth;

				void	(*CharTypedHandle						)		(VCLComponent*	me, wchar_t charValue						);

												VCLTextGrid			(VCL_TXTGRID_CREATE_PARAMS* inParams						);
												~VCLTextGrid		(															);

				virtual char					react		  		(unsigned int msg, unsigned int wParam, unsigned int lParam	) override;

				/*void							deleteColumn		(unsigned int inIndex										);
				void							insertColumn		(unsigned int inIndex, unsigned int inWidth					);

				void							deleteRow			(unsigned int inIndex										);
				void							insertRow			(unsigned int inIndex, unsigned int inHeight				);

				void							addToStartColumn	(unsigned int inWidth										);
				void							deleteFirstColumn	(															);*/

				void							addToStartRow		(unsigned int inHeight										);
				void							deleteFirstRow		(															);

				void							addToEndRow			(unsigned int inHeight										);
				void							deleteLastRow		(															);

				virtual void					focus				(															) override;
				virtual void					unfocus				(															) override;

				unsigned int					getRowCount			(															);
				unsigned int					getColCount			(															);

			protected:

				VCL_TEXT_CELL*	selectedCell;
				VCL_TEXT_ROW*	selectedRow;

				unsigned int	selX;
				unsigned int	selY;

				VCL_SIZE_VALUE*	widthList;
				VCL_SIZE_VALUE* lastWidth;

				VCL_SIZE_VALUE*	heightList;
				VCL_SIZE_VALUE* lastHeight;

				unsigned int	firstRowRender;
				unsigned int	firstCellRender;

				unsigned int	colCount;
				unsigned int	rowCount;

				unsigned int	fixRowCount;
				unsigned int	fixColCount;

				char			caretOn;
				char			caretBlinkOn;
				char			isThreadExist;

				HANDLE			caretRenderMutex	= CreateMutex(NULL, false, NULL);

				virtual void					rebuildSprite	(char realloc												) override;

				unsigned int					processCaret	(LPVOID	param												);
				static DWORD					newThreadMethod	(VCL_TXTGRID_THREAD_METHOD_PARAM*				mParam		);
		};

	class VCLColorDiagram : public VCLComponent
		{
			public:

				#define				VCL_CDIAG_PALETTE_WIDTH		256
				#define				VCL_CDIAG_PALETTE_HEIGHT	256
				#define				VCL_CDIAG_PALETTE_TOP		20
				#define				VCL_CDIAG_PALETTE_LEFT		20

				#define				VCL_CDIAG_BRUSH_WIDTH		10
				#define				VCL_CDIAG_BRUSH_HEIGHT		10
				#define				VCL_CDIAG_BRUSH_THICK		10

				#define				VCL_CDIAG_PALETTE_LEFT		20

				VCL_TYPED_HANDLE	bkColor;

				/*
				VCL_TYPED_HANDLE	fldBkColor;
				VCL_TYPED_HANDLE	fldTxtColor;
				VCL_TYPED_HANDLE	fldBrdColor;

				VCL_TYPED_HANDLE	fldBkSelColor;
				VCL_TYPED_HANDLE	fldTxtSelColor;
				VCL_TYPED_HANDLE	fldBrdSelColor;
				*/

				VCLString*			rCaption;
				int					rCapX;
				int					rCapY;

				VCLString*			gCaption;
				int					gCapX;
				int					gCapY;

				VCLString*			bCaption;
				int					bCapX;
				int					bCapY;

				VCLString*			aCaption;
				int					aCapX;
				int					aCapY;

				VCLTextField*		rField;
				VCLTextField*		gField;
				VCLTextField*		bField;
				VCLTextField*		aField;

				virtual void		unfocus			(															) override;
				virtual void		focus			(															) override;

			protected:

				VCLRenderQueue*		compList;

				virtual void		rebuildSprite	(char realloc												) override;
		};

#endif	//#ifndef __CPP_VCL__