#ifndef			__MAIN_WINDOW__

	#define		__MAIN_WINDOW__

	#include	"CPPVCL.h"

	int		__stdcall	WinMain		(
										HINSTANCE	hInst,
										HINSTANCE	hPreviousInst,
										LPSTR		lpCommandLine,
										int			nCommandShow
									);

#endif