#include "MainWindow.h"

VCLWindow*		mainWindow;
VCLTextGrid*	dbGrid;
VCLButton*		loadDBButton;
VCLButton*		saveDBButton;
VCLButton*		updateDBButton;
VCLButton*		graphButton;

HINSTANCE		mainInst;
int				nCmdShow;

int		__stdcall	WinMain		(
									HINSTANCE	hInst,
									HINSTANCE	hPreviousInst,
									LPSTR		lpCommandLine,
									int			nCommandShow
								)			
	{
		VCL_WINDOW_CREATE_PARAMS wc;

		VCLGenericColor* gc = new VCLGenericColor();

		gc->initGenericColor("/ * 255 + x y + width height", "/ * 255 + x y + width height", "/ * 255 + x y + width height", "255");

		RGBAPix* rgbac	= new RGBAPix{255, 200, 150, 0};

		wc.hInst				= hInst;
		wc.wndStyle				= WS_OVERLAPPEDWINDOW;
		wc.nCommandShow			= nCommandShow;
		wc.wndClassName			= L"tstWnd";
		wc.wndCaption			= L"TEST";
		wc.wndBkColor.type		= VCL_HT_RGBA_COLOR;
		wc.wndBkColor.handle	= rgbac;
		wc.wndHeight			= 600;
		wc.wndWidth				= 800;
		wc.wndX					= 100;
		wc.wndY					= 100;
		
		mainWindow				= new VCLWindow(&wc);

		VCL_BUTTON_CREATE_PARAMS bc;

		bc.parentWindow			= mainWindow->hWindow;
		bc.parentComponent		= NULL;
		bc.renderList			= mainWindow->compList;
		bc.capX					= 0;
		bc.capY					= 0;
		bc.btnX					= 100;
		bc.btnY					= 100;
		bc.btnWidth				= 200;
		bc.btnHeight			= 50;
		bc.btnBorderWidth		= 10;
		bc.btnVisible			= 1;
		bc.btnCaption			= new VCLString(L"Button");
		bc.btnCapSelColor.type	= VCL_HT_RGBA_COLOR;
		bc.btnCapSelColor.handle= new RGBAPix{ 255, 0, 0, 255};
		bc.btnCapColor.type		= VCL_HT_RGBA_COLOR;
		bc.btnCapColor.handle	= new RGBAPix{0, 0, 0, 255};
		bc.btnBkColor.type		= VCL_HT_RGBA_COLOR;
		bc.btnBkColor.handle	= new RGBAPix{ 100, 100, 50, 180};
		bc.btnBkSelColor.type	= VCL_HT_RGBA_COLOR;
		bc.btnBkSelColor.handle	= new RGBAPix{100, 100, 100, 255};
		bc.btnBrdColor.type		= VCL_HT_RGBA_COLOR;
		bc.btnBrdColor.handle	= new RGBAPix{ 255, 50, 50, 100};

		VCLButton* bt			= new VCLButton(&bc);

		VCL_TXTGRID_CREATE_PARAMS params;

		params.parentWindow			= mainWindow->hWindow;
		params.parentComponent		= NULL;
		params.renderList			= mainWindow->compList;
		params.grdX					= 0;
		params.grdY					= 0;
		params.grdRowCount			= 50;
		params.grdColCount			= 50;
		params.grdFixRowCount		= 3;
		params.grdFixColCount		= 3;
		params.grdInitialCellWidth	= 20;
		params.grdInitialCellHeight	= 20;

		params.grdBkColor.type		= VCL_HT_RGBA_COLOR;
		params.grdBkColor.handle	= new RGBAPix{100, 255, 100, 230};
		
		params.grdGrdColor.type		= VCL_HT_RGBA_COLOR;
		params.grdGrdColor.handle	= new RGBAPix{255, 100, 255, 255};

		params.grdFixBkColor.type	= VCL_HT_RGBA_COLOR;
		params.grdFixBkColor.handle	= new RGBAPix{0, 0, 0, 230};

		//params.grdBkMarkColor		= 

		params.grdCapColor.type		= VCL_HT_RGBA_COLOR;
		params.grdCapColor.handle	= new RGBAPix{0, 0, 0, 230};

		params.grdCapMarkColor.type		= VCL_HT_RGBA_COLOR;
		params.grdCapMarkColor.handle	= new RGBAPix{0, 0, 0, 230};

		params.grdCapSelColor.type		= VCL_HT_RGBA_COLOR;
		params.grdCapSelColor.handle	= new RGBAPix{0, 0, 0, 230};

		params.grdWidth				= 800;
		params.grdHeight			= 600;
		params.grdGridWidth			= 2;
		params.grdVisible			= 1;
		
		VCLTextGrid* grd			= new VCLTextGrid(&params);

		mainWindow->windowLiveCycle();

		return 0;
	}